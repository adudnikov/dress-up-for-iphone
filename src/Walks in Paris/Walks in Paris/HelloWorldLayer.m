//
//  HelloWorldLayer.m
//  Walks in Paris
//
//  Created by Andrey Zavaliy on 28.12.12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//


// Import the interfaces
#import "HelloWorldLayer.h"

// Needed to obtain the Navigation Controller
#import "AppDelegate.h"
#import "SimpleAudioEngine.h"
#import "Singleton.h"
#import "CCScreenshot.h"
#import "MailSender.h"
#import "ChoiceModel.h"
#import "InAppRageIAPHelper.h"
#import "Reachability.h"
#import "Flurry.h"
#import "FlurryAds.h"
#import "FlurryAdDelegate.h"
#import "GameConfig.h"

#pragma mark - HelloWorldLayer

// HelloWorldLayer implementation
@implementation HelloWorldLayer

// Helper class method that creates a Scene with the HelloWorldLayer as the only child.
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	HelloWorldLayer *layer = [HelloWorldLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super's" return value
	if( (self=[super init]) ) {
		winsize = [[CCDirector sharedDirector] winSize];
		
		[[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"main1.plist"];
        [[CCTextureCache sharedTextureCache] addImage:@"main1.pvr.ccz" ];
        
        CCSprite *background1 = [CCSprite spriteWithSpriteFrameName:@"back1.png"];
        background1.position = ccp(winsize.width/2, winsize.height/2);
        [self addChild:background1 z:0 tag:1];
        
        CCSprite *background2 = [CCSprite spriteWithSpriteFrameName:@"back2.png"];
        background2.position = ccp(winsize.width/2, winsize.height/2);
        [self addChild:background2 z:0 tag:2];
        background2.opacity=0;
        
        CCSprite *dim = [CCSprite spriteWithSpriteFrameName:@"dim1.png"];
        dim.position=ccp(winsize.width/2-117.5, winsize.height/2-30.1);
        [background2 addChild:dim z:1 tag:7];
        dim.opacity=0;
        
        NSMutableArray *animFrames2 = [NSMutableArray array];
        for(int i = 1; i < 8; i++) {
            
            CCSpriteFrame *frame2 = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"dim%d.png",i]];
            [animFrames2 addObject:frame2];
        }
        
        CCAnimation *animation2 = [CCAnimation animationWithSpriteFrames:animFrames2 delay:0.1f];
        CCAction *dimAction = [CCRepeatForever actionWithAction: [CCAnimate actionWithAnimation:animation2]];
        [dim runAction:dimAction];
        
        CCSprite *background3 = [CCSprite spriteWithSpriteFrameName:@"back3.png"];
        background3.position = ccp(winsize.width/2, winsize.height-background3.contentSize.height/2);
        [self addChild:background3 z:0 tag:3];
        background3.opacity=0;
        
        CCSprite *temp1 = [CCSprite spriteWithSpriteFrameName:@"home_button_released.png"];
        CCSprite *temp2 = [CCSprite spriteWithSpriteFrameName:@"home_button_pressed.png"];
        
        CCMenuItemSprite *home_button = [CCMenuItemSprite itemWithNormalSprite:temp1 selectedSprite:temp2 target:self selector:@selector(gobackscene)];
        int i = winsize.height-home_button.contentSize.height/2 - 10;
        home_button.position = ccp(home_button.contentSize.width/2+5, i);
        
        temp1 = [CCSprite spriteWithSpriteFrameName:@"back_button_released.png"];
        temp2 = [CCSprite spriteWithSpriteFrameName:@"back_button_pressed.png"];
        
        CCMenuItemSprite *background_button = [CCMenuItemSprite itemWithNormalSprite:temp1 selectedSprite:temp2 target:self selector:@selector(changebackground)];
        i = i-background_button.contentSize.height/2-home_button.contentSize.height/2;
        background_button.position = ccp(background_button.contentSize.width/2+5, i);
        
        temp1 = [CCSprite spriteWithSpriteFrameName:@"share_button_released.png"];
        temp2 = [CCSprite spriteWithSpriteFrameName:@"share_button_pressed.png"];
        
        CCMenuItemSprite *facebook_button = [CCMenuItemSprite itemWithNormalSprite:temp1 selectedSprite:temp2 target:self selector:@selector(sharephoto)];
        i = i-background_button.contentSize.height/2-facebook_button.contentSize.height/2;
        facebook_button.position = ccp(facebook_button.contentSize.width/2+5, i);
        
        temp1 = [CCSprite spriteWithSpriteFrameName:@"photo_button_released.png"];
        temp2 = [CCSprite spriteWithSpriteFrameName:@"photo_button_pressed.png"];
        
        CCMenuItemSprite *photo_button = [CCMenuItemSprite itemWithNormalSprite:temp1 selectedSprite:temp2 target:self selector:@selector(captureToPhotoAlbum)];
        i = i-facebook_button.contentSize.height/2-photo_button.contentSize.height/2;
        photo_button.position = ccp(photo_button.contentSize.width/2+5, i);
        
        temp1 = [CCSprite spriteWithSpriteFrameName:@"reset_button_released.png"];
        temp2 = [CCSprite spriteWithSpriteFrameName:@"reset_button_pressed.png"];
        
        CCMenuItemSprite *reset_button = [CCMenuItemSprite itemWithNormalSprite:temp1 selectedSprite:temp2 target:self selector:@selector(resetgame)];
        i = i-photo_button.contentSize.height/2-reset_button.contentSize.height/2;
        reset_button.position = ccp(reset_button.contentSize.width/2+5, i);
        
        mainmenu = [CCMenu menuWithItems:home_button, background_button, facebook_button, photo_button, reset_button, nil];
        mainmenu.position = CGPointZero;
        [self addChild:mainmenu z:301];
        
        float delayTime = 0.3f;
        
        for (CCMenuItemFont *each in [mainmenu children]) {
            each.scaleX = 0.0f;
            each.scaleY = 0.0f;
            CCAction *action = [CCSequence actions:
                                [CCDelayTime actionWithDuration: delayTime],
                                [CCScaleTo actionWithDuration:0.5F scale:1.0],
                                nil];
            delayTime += 0.2f;
            [each runAction: action];
        }
        
        size_button=41;
        between_button_x=4;
        between_button_y=2;
        
        movablebutton = [[NSMutableArray alloc] init];
        
        CCSprite *mySprite = [CCSprite spriteWithSpriteFrameName:@"scarfs_released.png"];
        i=winsize.height-2*between_button_y/*-size_button/2*/;
        mySprite.position = ccp(winsize.width-between_button_x-size_button/2,i);
        [self addChild:mySprite z:300 tag:11];
        temp1 = [CCSprite spriteWithSpriteFrameName:@"scarfs_pressed.png"];
        temp1.opacity=0;
        temp1.position= ccp(mySprite.contentSize.width/2,mySprite.contentSize.height/2);
        [mySprite addChild:temp1 z:1 tag:31];
        temp2 = [CCSprite spriteWithSpriteFrameName:@"scarfs_active.png"];
        temp2.opacity=0;
        temp2.position= ccp(mySprite.contentSize.width/2,mySprite.contentSize.height/2);
        [mySprite addChild:temp2 z:1 tag:51];
        [movablebutton addObject:mySprite];
        
        mySprite = [CCSprite spriteWithSpriteFrameName:@"bags_released.png"];
        i=i-between_button_y-size_button;
        mySprite.position = ccp(winsize.width-between_button_x-size_button/2,i);
        [self addChild:mySprite z:300 tag:12];
        temp1 = [CCSprite spriteWithSpriteFrameName:@"bags_pressed.png"];
        temp1.opacity=0;
        temp1.position= ccp(mySprite.contentSize.width/2,mySprite.contentSize.height/2);
        [mySprite addChild:temp1 z:1 tag:32];
        temp2 = [CCSprite spriteWithSpriteFrameName:@"bags_active.png"];
        temp2.opacity=0;
        temp2.position= ccp(mySprite.contentSize.width/2,mySprite.contentSize.height/2);
        [mySprite addChild:temp2 z:1 tag:52];
        [movablebutton addObject:mySprite];      
        
        mySprite = [CCSprite spriteWithSpriteFrameName:@"belts_released.png"];
        i=i-between_button_y-size_button;
        mySprite.position = ccp(winsize.width-between_button_x-size_button/2,i);
        [self addChild:mySprite z:300 tag:13];
        temp1 = [CCSprite spriteWithSpriteFrameName:@"belts_pressed.png"];
        temp1.opacity=0;
        temp1.position= ccp(mySprite.contentSize.width/2,mySprite.contentSize.height/2);
        [mySprite addChild:temp1 z:1 tag:33];
        temp2 = [CCSprite spriteWithSpriteFrameName:@"belts_active.png"];
        temp2.opacity=0;
        temp2.position= ccp(mySprite.contentSize.width/2,mySprite.contentSize.height/2);
        [mySprite addChild:temp2 z:1 tag:53];
        [movablebutton addObject:mySprite];
        
        mySprite = [CCSprite spriteWithSpriteFrameName:@"jew_released.png"];
        i=i-between_button_y-size_button;
        mySprite.position = ccp(winsize.width-between_button_x-size_button/2,i);
        [self addChild:mySprite z:300 tag:14];
        temp1 = [CCSprite spriteWithSpriteFrameName:@"jew_pressed.png"];
        temp1.opacity=0;
        temp1.position= ccp(mySprite.contentSize.width/2,mySprite.contentSize.height/2);
        [mySprite addChild:temp1 z:1 tag:34];
        temp2 = [CCSprite spriteWithSpriteFrameName:@"jew_active.png"];
        temp2.opacity=0;
        temp2.position= ccp(mySprite.contentSize.width/2,mySprite.contentSize.height/2);
        [mySprite addChild:temp2 z:1 tag:54];
        [movablebutton addObject:mySprite];

        mySprite = [CCSprite spriteWithSpriteFrameName:@"hair_released.png"];
        i=i-between_button_y-size_button;
        mySprite.position = ccp(winsize.width-between_button_x-size_button/2,i);
        [self addChild:mySprite z:300 tag:15];
        temp1 = [CCSprite spriteWithSpriteFrameName:@"hair_pressed.png"];
        temp1.opacity=0;
        temp1.position= ccp(mySprite.contentSize.width/2,mySprite.contentSize.height/2);
        [mySprite addChild:temp1 z:1 tag:35];
        temp2 = [CCSprite spriteWithSpriteFrameName:@"hair_active.png"];
        temp2.opacity=0;
        temp2.position= ccp(mySprite.contentSize.width/2,mySprite.contentSize.height/2);
        [mySprite addChild:temp2 z:1 tag:55];
        [movablebutton addObject:mySprite];
        
        mySprite = [CCSprite spriteWithSpriteFrameName:@"glasses_released.png"];
        i=i-between_button_y-size_button;
        mySprite.position = ccp(winsize.width-between_button_x-size_button/2,i);
        [self addChild:mySprite z:300 tag:16];
        temp1 = [CCSprite spriteWithSpriteFrameName:@"glasses_pressed.png"];
        temp1.opacity=0;
        temp1.position= ccp(mySprite.contentSize.width/2,mySprite.contentSize.height/2);
        [mySprite addChild:temp1 z:1 tag:36];
        temp2 = [CCSprite spriteWithSpriteFrameName:@"glasses_active.png"];
        temp2.opacity=0;
        temp2.position= ccp(mySprite.contentSize.width/2,mySprite.contentSize.height/2);
        [mySprite addChild:temp2 z:1 tag:56];
        [movablebutton addObject:mySprite];
        
        mySprite = [CCSprite spriteWithSpriteFrameName:@"hats_released.png"];
        i=i-between_button_y-size_button;
        mySprite.position = ccp(winsize.width-between_button_x-size_button/2,i);
        [self addChild:mySprite z:300 tag:17];
        temp1 = [CCSprite spriteWithSpriteFrameName:@"hats_pressed.png"];
        temp1.opacity=0;
        temp1.position= ccp(mySprite.contentSize.width/2,mySprite.contentSize.height/2);
        [mySprite addChild:temp1 z:1 tag:37];
        temp2 = [CCSprite spriteWithSpriteFrameName:@"hats_active.png"];
        temp2.opacity=0;
        temp2.position= ccp(mySprite.contentSize.width/2,mySprite.contentSize.height/2);
        [mySprite addChild:temp2 z:1 tag:57];
        [movablebutton addObject:mySprite];
        
        mySprite = [CCSprite spriteWithSpriteFrameName:@"jackets_released.png"];
        i=i-between_button_y-size_button;
        mySprite.position = ccp(winsize.width-between_button_x-size_button/2,i);
        [self addChild:mySprite z:300 tag:18];
        temp1 = [CCSprite spriteWithSpriteFrameName:@"jackets_pressed.png"];
        temp1.opacity=0;
        temp1.position= ccp(mySprite.contentSize.width/2,mySprite.contentSize.height/2);
        [mySprite addChild:temp1 z:1 tag:38];
        temp2 = [CCSprite spriteWithSpriteFrameName:@"jackets_active.png"];
        temp2.opacity=0;
        temp2.position= ccp(mySprite.contentSize.width/2,mySprite.contentSize.height/2);
        [mySprite addChild:temp2 z:1 tag:58];
        [movablebutton addObject:mySprite];
        
        mySprite = [CCSprite spriteWithSpriteFrameName:@"tops_released.png"];
        i=i-between_button_y-size_button;
        mySprite.position = ccp(winsize.width-between_button_x-size_button/2,i);
        [self addChild:mySprite z:300 tag:19];
        temp1 = [CCSprite spriteWithSpriteFrameName:@"tops_pressed.png"];
        temp1.opacity=0;
        temp1.position= ccp(mySprite.contentSize.width/2,mySprite.contentSize.height/2);
        [mySprite addChild:temp1 z:1 tag:39];
        temp2 = [CCSprite spriteWithSpriteFrameName:@"tops_active.png"];
        temp2.opacity=0;
        temp2.position= ccp(mySprite.contentSize.width/2,mySprite.contentSize.height/2);
        [mySprite addChild:temp2 z:1 tag:59];
        [movablebutton addObject:mySprite];
        
        mySprite = [CCSprite spriteWithSpriteFrameName:@"pants_released.png"];
        i=i-between_button_y-size_button;
        mySprite.position = ccp(winsize.width-between_button_x-size_button/2,i);
        [self addChild:mySprite z:300 tag:20];
        temp1 = [CCSprite spriteWithSpriteFrameName:@"pants_pressed.png"];
        temp1.opacity=0;
        temp1.position= ccp(mySprite.contentSize.width/2,mySprite.contentSize.height/2);
        [mySprite addChild:temp1 z:1 tag:40];
        temp2 = [CCSprite spriteWithSpriteFrameName:@"pants_active.png"];
        temp2.opacity=0;
        temp2.position= ccp(mySprite.contentSize.width/2,mySprite.contentSize.height/2);
        [mySprite addChild:temp2 z:1 tag:60];
        [movablebutton addObject:mySprite];
        
        mySprite = [CCSprite spriteWithSpriteFrameName:@"skirts_released.png"];
        i=i-between_button_y-size_button;
        mySprite.position = ccp(winsize.width-between_button_x-size_button/2,i);
        [self addChild:mySprite z:300 tag:21];
        temp1 = [CCSprite spriteWithSpriteFrameName:@"skirts_pressed.png"];
        temp1.opacity=0;
        temp1.position= ccp(mySprite.contentSize.width/2,mySprite.contentSize.height/2);
        [mySprite addChild:temp1 z:1 tag:41];
        temp2 = [CCSprite spriteWithSpriteFrameName:@"skirts_active.png"];
        temp2.opacity=0;
        temp2.position= ccp(mySprite.contentSize.width/2,mySprite.contentSize.height/2);
        [mySprite addChild:temp2 z:1 tag:61];
        [movablebutton addObject:mySprite];
        
        mySprite = [CCSprite spriteWithSpriteFrameName:@"dresses_released.png"];
        i=i-between_button_y-size_button;
        mySprite.position = ccp(winsize.width-between_button_x-size_button/2,i);
        [self addChild:mySprite z:300 tag:22];
        temp1 = [CCSprite spriteWithSpriteFrameName:@"dresses_pressed.png"];
        temp1.opacity=0;
        temp1.position= ccp(mySprite.contentSize.width/2,mySprite.contentSize.height/2);
        [mySprite addChild:temp1 z:1 tag:42];
        temp2 = [CCSprite spriteWithSpriteFrameName:@"dresses_active.png"];
        temp2.opacity=0;
        temp2.position= ccp(mySprite.contentSize.width/2,mySprite.contentSize.height/2);
        [mySprite addChild:temp2 z:1 tag:62];
        [movablebutton addObject:mySprite];
        
        mySprite = [CCSprite spriteWithSpriteFrameName:@"stockings_released.png"];
        i=i-between_button_y-size_button;
        mySprite.position = ccp(winsize.width-between_button_x-size_button/2,i);
        [self addChild:mySprite z:300 tag:23];
        temp1 = [CCSprite spriteWithSpriteFrameName:@"stockings_pressed.png"];
        temp1.opacity=0;
        temp1.position= ccp(mySprite.contentSize.width/2,mySprite.contentSize.height/2);
        [mySprite addChild:temp1 z:1 tag:43];
        temp2 = [CCSprite spriteWithSpriteFrameName:@"stockings_active.png"];
        temp2.opacity=0;
        temp2.position= ccp(mySprite.contentSize.width/2,mySprite.contentSize.height/2);
        [mySprite addChild:temp2 z:1 tag:63];
        [movablebutton addObject:mySprite];       
        
        mySprite = [CCSprite spriteWithSpriteFrameName:@"shoes_released.png"];
        i=i-between_button_y-size_button;
        mySprite.position = ccp(winsize.width-between_button_x-size_button/2,i);
        [self addChild:mySprite z:300 tag:24];
        temp1 = [CCSprite spriteWithSpriteFrameName:@"shoes_pressed.png"];
        temp1.opacity=0;
        temp1.position= ccp(mySprite.contentSize.width/2,mySprite.contentSize.height/2);
        [mySprite addChild:temp1 z:1 tag:44];
        temp2 = [CCSprite spriteWithSpriteFrameName:@"shoes_active.png"];
        temp2.opacity=0;
        temp2.position= ccp(mySprite.contentSize.width/2,mySprite.contentSize.height/2);
        [mySprite addChild:temp2 z:1 tag:64];
        [movablebutton addObject:mySprite];
        
        switch ([Singleton sharedSingleron].number_model) {
            case 1:
            {
                CCSprite *model = [CCSprite spriteWithSpriteFrameName:@"model1.png"];
                model.position = ccp(155,280); //ccp(777, 432);
                [self addChild:model z:3 tag:10];
                
                CCSprite *hand = [CCSprite spriteWithSpriteFrameName:@"model1_ruka.png"];
                hand.position = ccp(155,280); //136,322
                [self addChild:hand z:164 tag:9];
                
                CCSprite *hand2 = [CCSprite spriteWithSpriteFrameName:@"model1_ruka2.png"];
                hand2.position = ccp(155,280); //136,322
                [self addChild:hand2 z:71 tag:8];
                hand2.visible=NO;
            }
                break;
            case 2:
            {
                CCSprite *model = [CCSprite spriteWithSpriteFrameName:@"model2.png"];
                model.position = ccp(155,280); //ccp(777, 432);
                [self addChild:model z:3 tag:10];
                
                CCSprite *hand = [CCSprite spriteWithSpriteFrameName:@"model2_ruka.png"];
                hand.position = ccp(155,280); //136,322
                [self addChild:hand z:164 tag:9];
                
                CCSprite *hand2 = [CCSprite spriteWithSpriteFrameName:@"model2_ruka2.png"];
                hand2.position = ccp(155,280); //136,322
                [self addChild:hand2 z:71 tag:8];
                hand2.visible=NO;
            }
                break;
            case 3:
            {
                CCSprite *model = [CCSprite spriteWithSpriteFrameName:@"model3.png"];
                model.position = ccp(155,280); //ccp(777, 432);
                [self addChild:model z:3 tag:10];
                
                CCSprite *hand = [CCSprite spriteWithSpriteFrameName:@"model3_ruka.png"];
                hand.position = ccp(155,280); //136,322
                [self addChild:hand z:164 tag:9];
                
                CCSprite *hand2 = [CCSprite spriteWithSpriteFrameName:@"model3_ruka2.png"];
                hand2.position = ccp(155,280); //136,322
                [self addChild:hand2 z:71 tag:8];
                hand2.visible=NO;
            }
                break;
                
            default:
                break;
        }
        [[CCTextureCache sharedTextureCache] removeUnusedTextures];
        [[CCSpriteFrameCache sharedSpriteFrameCache] removeUnusedSpriteFrames];
        
        /*[[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"inapp_facebook.plist"];
        [[CCTextureCache sharedTextureCache] addImage:@"inapp_facebook.pvr.ccz" ];
        
        maindialog = [CCSprite spriteWithSpriteFrameName:@"back_red.png"];
        maindialog.position = ccp(-winsize.width/2, -winsize.height/2);
        [self addChild:maindialog z:1000];
        temp1 = [CCSprite spriteWithSpriteFrameName:@"facebook_released.png"];
        temp2 = [CCSprite spriteWithSpriteFrameName:@"facebook_pressed.png"];
        CCMenuItemSprite *buttonforfacebook = [CCMenuItemSprite itemWithNormalSprite:temp1 selectedSprite:temp2 target:self selector:@selector(sendfacebook)];
        buttonforfacebook.position = ccp(maindialog.contentSize.width/2,maindialog.contentSize.height*11/20);
        temp1 = [CCSprite spriteWithSpriteFrameName:@"email_released.png"];
        temp2 = [CCSprite spriteWithSpriteFrameName:@"email_pressed.png"];
        CCMenuItemSprite *buttonformail = [CCMenuItemSprite itemWithNormalSprite:temp1 selectedSprite:temp2 target:self selector:@selector(sendmail)];
        buttonformail.position = ccp(maindialog.contentSize.width/2,buttonforfacebook.position.y-buttonforfacebook.contentSize.height*11/10);
        titledialog = [CCLabelTTF labelWithString:NSLocalizedString(@"Share", nil) fontName:@"Helvetica-Bold" fontSize:24];
        titledialog.position = ccp(maindialog.contentSize.width/2, buttonforfacebook.position.y+buttonforfacebook.contentSize.height*11/10);
        [titledialog setColor:ccWHITE];
        [maindialog addChild:titledialog];
        maindialogmenu = [CCMenu menuWithItems:buttonforfacebook, buttonformail, nil];
        maindialogmenu.position = CGPointZero;
        [maindialog addChild:maindialogmenu];
        
        rezultdialog = [CCSprite spriteWithSpriteFrameName:@"back_blue.png"];
        rezultdialog.position = ccp(-winsize.width/2, -winsize.height/2);
        [self addChild:rezultdialog z:1000];
        
        messagerezult = [CCLabelTTF labelWithString:NSLocalizedString(@"Would you like to buy\nadditional items?", nil) dimensions:CGSizeMake(300, 70) hAlignment:NSTextAlignmentCenter vAlignment:NSTextAlignmentCenter fontName:@"Helvetica" fontSize:15 ];
        messagerezult.position = ccp(rezultdialog.contentSize.width/2, rezultdialog.contentSize.height*0.75*19/20);
        [messagerezult setColor:ccWHITE];
        [rezultdialog addChild:messagerezult];
        
        temp1 = [CCSprite spriteWithSpriteFrameName:@"ok_released_trial.png"];
        temp2 = [CCSprite spriteWithSpriteFrameName:@"ok_pressed_trial.png"];
        
        CCMenuItemSprite *button_ok_trial1 = [CCMenuItemSprite itemWithNormalSprite:temp1 selectedSprite:temp2 target:self selector:@selector(okeypressedtrial)];
        button_ok_trial1.position = ccp(rezultdialog.contentSize.width/2+14*0.47+button_ok_trial1.contentSize.width/2,rezultdialog.contentSize.height/2+12*0.47);
        button_ok_trial = [CCMenu menuWithItems:button_ok_trial1, nil];
        button_ok_trial.position = CGPointZero;
        [rezultdialog addChild:button_ok_trial];
        
        temp1 = [CCSprite spriteWithSpriteFrameName:NSLocalizedString(@"cancel_released_trial.png", nil)];
        temp2 = [CCSprite spriteWithSpriteFrameName:NSLocalizedString(@"cancel_pressed_trial.png", nil)];
        
        CCMenuItemSprite *button_cancel_trial1 = [CCMenuItemSprite itemWithNormalSprite:temp1 selectedSprite:temp2 target:self selector:@selector(cancelpressedtrial)];
        button_cancel_trial1.position = ccp(rezultdialog.contentSize.width/2-button_cancel_trial1.contentSize.width/2-15*0.47,rezultdialog.contentSize.height/2+12*0.47);
        button_cancel_trial = [CCMenu menuWithItems:button_cancel_trial1, nil];
        button_cancel_trial.position = CGPointZero;
        [rezultdialog addChild:button_cancel_trial];
        
        temp1 = [CCSprite spriteWithSpriteFrameName:NSLocalizedString(@"restore_purchase_released.png", nil)];
        temp2 = [CCSprite spriteWithSpriteFrameName:NSLocalizedString(@"restore_purchase_pressed.png", nil)];
        
        CCMenuItemSprite *button_restore_trial1 = [CCMenuItemSprite itemWithNormalSprite:temp1 selectedSprite:temp2 target:self selector:@selector(restorepressedtrial)];
        
        button_restore_trial1.position = ccp(rezultdialog.contentSize.width/2, rezultdialog.contentSize.height/3);
        button_restore_trial = [CCMenu menuWithItems:button_restore_trial1, nil];
        button_restore_trial.position = CGPointZero;
        [rezultdialog addChild:button_restore_trial];*/
        
        CCSprite *panel_elements = [CCSprite spriteWithFile:@"blank.png"];
		[panel_elements setTextureRect:CGRectMake(0,0,winsize.width,90)];
        panel_elements.position = ccp(winsize.width/2, panel_elements.contentSize.height/2);
		[panel_elements setColor:ccc3(184,180,179)];
		[self addChild:panel_elements z:310 tag:5];
        
        level_item = [[NSMutableArray alloc] init];
        
        movableelements = [[NSMutableArray alloc] init];
        
        x_maxmenu = 70;
        x_between = 10;
        
        //[[CCTextureCache sharedTextureCache] removeUnusedTextures];
        //[[CCSpriteFrameCache sharedSpriteFrameCache] removeUnusedSpriteFrames];
        
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"obj1.plist"];
        [[CCTextureCache sharedTextureCache] addImage:@"obj1.pvr.ccz" ];
        
        for(int i = 0; i < 12; ++i) {
            NSString *image = [NSString stringWithFormat:@"scarf%d.png", i+1];;
            mySprite = [CCSprite spriteWithSpriteFrameName:image];
            mySprite.position = CGPointMake((float)(i+0.5)*x_maxmenu+(i+0.5)*x_between, panel_elements.contentSize.height/2);
            if (mySprite.contentSize.width>mySprite.contentSize.height) {
                if (x_maxmenu/mySprite.contentSize.width<1) {mySprite.scale = (float)x_maxmenu/mySprite.contentSize.width;}
                else {mySprite.scale =1;}
            } else {
                if (x_maxmenu/mySprite.contentSize.height<1) {mySprite.scale = (float)x_maxmenu/mySprite.contentSize.height;}
                else {mySprite.scale =1;}
            }
            [self addChild:mySprite z:311 tag:101+i];
            [movableelements addObject:mySprite];
            mySprite.visible=NO;
            [level_item addObject:[NSNumber numberWithInteger:150]];
            if ([Singleton sharedSingleron].Trial_Mode==YES) {
                if (i>2) {
                    CCSprite *zamok = [CCSprite spriteWithSpriteFrameName:@"zamok.png"];
                    zamok.position = ccp(mySprite.contentSize.width/2, mySprite.contentSize.height/2);
                    zamok.scale=1/mySprite.scale;
                    [mySprite addChild:zamok z:1 tag:10101+i];
                }
            }
        }
        
        for(int i = 0; i < 17; ++i) {
            NSString *image = [NSString stringWithFormat:@"bag%d.png", i+1];
            mySprite = [CCSprite spriteWithSpriteFrameName:image];
            mySprite.position = CGPointMake((float)(i+0.5)*x_maxmenu+(i+0.5)*x_between, panel_elements.contentSize.height/2);
            if (mySprite.contentSize.width>mySprite.contentSize.height) {
                if (x_maxmenu/mySprite.contentSize.width<1) {mySprite.scale = (float)x_maxmenu/mySprite.contentSize.width;}
                else {mySprite.scale =1;}
            } else {
                if (x_maxmenu/mySprite.contentSize.height<1) {mySprite.scale = (float)x_maxmenu/mySprite.contentSize.height;}
                else {mySprite.scale =1;}
            }
            [self addChild:mySprite z:311 tag:113+i];
            [movableelements addObject:mySprite];
            mySprite.visible=NO;
            [level_item addObject:[NSNumber numberWithInteger:163]];
            if ([Singleton sharedSingleron].Trial_Mode==YES) {
                if (i>2) {
                    CCSprite *zamok = [CCSprite spriteWithSpriteFrameName:@"zamok.png"];
                    zamok.position = ccp(mySprite.contentSize.width/2, mySprite.contentSize.height/2);
                    zamok.scale=1/mySprite.scale;
                    [mySprite addChild:zamok z:1 tag:10113+i];
                }
            }
        }
        
        for(int i = 0; i < 8; ++i) {
            NSString *image = [NSString stringWithFormat:@"belt%d.png", i+1];
            mySprite = [CCSprite spriteWithSpriteFrameName:image];
            mySprite.position = CGPointMake((float)(i+0.5)*x_maxmenu+(i+0.5)*x_between, panel_elements.contentSize.height/2);
            if (mySprite.contentSize.width>mySprite.contentSize.height) {
                if (x_maxmenu/mySprite.contentSize.width<1) {mySprite.scale = (float)x_maxmenu/mySprite.contentSize.width;}
                else {mySprite.scale =1;}
            } else {
                if (x_maxmenu/mySprite.contentSize.height<1) {mySprite.scale = (float)x_maxmenu/mySprite.contentSize.height;}
                else {mySprite.scale =1;}
            }
            [self addChild:mySprite z:311 tag:130+i];
            [movableelements addObject:mySprite];
            mySprite.visible=NO;
            [level_item addObject:[NSNumber numberWithInteger:80]];
            if ([Singleton sharedSingleron].Trial_Mode==YES) {
                if (i>2) {
                    CCSprite *zamok = [CCSprite spriteWithSpriteFrameName:@"zamok.png"];
                    zamok.position = ccp(mySprite.contentSize.width/2, mySprite.contentSize.height/2);
                    zamok.scale=1/mySprite.scale;
                    [mySprite addChild:zamok z:1 tag:10130+i];
                }
            }
        }
        
        for(int i = 0; i < 14; ++i) {
            NSString *image = [NSString stringWithFormat:@"jew%d.png", i+1];
            mySprite = [CCSprite spriteWithSpriteFrameName:image];
            mySprite.position = CGPointMake((float)(i+0.5)*x_maxmenu+(i+0.5)*x_between, panel_elements.contentSize.height/2);
            if (mySprite.contentSize.width>mySprite.contentSize.height) {
                if (x_maxmenu/mySprite.contentSize.width<1) {mySprite.scale = (float)x_maxmenu/mySprite.contentSize.width;}
                else {mySprite.scale =1;}
            } else {
                if (x_maxmenu/mySprite.contentSize.height<1) {mySprite.scale = (float)x_maxmenu/mySprite.contentSize.height;}
                else {mySprite.scale =1;}
            }
            [self addChild:mySprite z:311 tag:138+i];
            [movableelements addObject:mySprite];
            mySprite.visible=NO;
            if ((i<10)&&(i!=7)) {[level_item addObject:[NSNumber numberWithInteger:90]];} else {
                if (i>11) {[level_item addObject:[NSNumber numberWithInteger:165]];} else {
                    [level_item addObject:[NSNumber numberWithInteger:140]];
                }
            }
            if ([Singleton sharedSingleron].Trial_Mode==YES) {
                if (i>2) {
                    CCSprite *zamok = [CCSprite spriteWithSpriteFrameName:@"zamok.png"];
                    zamok.position = ccp(mySprite.contentSize.width/2, mySprite.contentSize.height/2);
                    zamok.scale=1/mySprite.scale;
                    [mySprite addChild:zamok z:1 tag:10138+i];
                }
            }
        }
        
        for(int i = 0; i < 32; ++i) {
            NSString *image = [NSString stringWithFormat:@"hair%d.png", i+1];
            mySprite = [CCSprite spriteWithSpriteFrameName:image];
            mySprite.position = CGPointMake((float)(i+0.5)*x_maxmenu+(i+0.5)*x_between, panel_elements.contentSize.height/2);
            if (mySprite.contentSize.width>mySprite.contentSize.height) {
                if (x_maxmenu/mySprite.contentSize.width<1) {mySprite.scale = (float)x_maxmenu/mySprite.contentSize.width;}
                else {mySprite.scale =1;}
            } else {
                if (x_maxmenu/mySprite.contentSize.height<1) {mySprite.scale = (float)x_maxmenu/mySprite.contentSize.height;}
                else {mySprite.scale =1;}
            }
            [self addChild:mySprite z:311 tag:152+i];
            [movableelements addObject:mySprite];
            mySprite.visible=NO;
            if ([Singleton sharedSingleron].Trial_Mode==YES) {
                if (i>2) {
                    CCSprite *zamok = [CCSprite spriteWithSpriteFrameName:@"zamok.png"];
                    zamok.position = ccp(mySprite.contentSize.width/2, mySprite.contentSize.height/2);
                    zamok.scale=1/mySprite.scale;
                    [mySprite addChild:zamok z:1 tag:10152+i];
                }
            }
            if ((i==1)||(i==12)||(i==23)) {[level_item addObject:[NSNumber numberWithInteger:161]];} else {[level_item addObject:[NSNumber numberWithInteger:160]];}
        }
        
        for(int i = 0; i < 7; ++i) {
            NSString *image = [NSString stringWithFormat:@"glasses%d.png", i+1];
            mySprite = [CCSprite spriteWithSpriteFrameName:image];
            mySprite.position = CGPointMake((float)(i+0.5)*x_maxmenu+(i+0.5)*x_between, panel_elements.contentSize.height/2);
            if (mySprite.contentSize.width>mySprite.contentSize.height) {
                if (x_maxmenu/mySprite.contentSize.width<1) {mySprite.scale = (float)x_maxmenu/mySprite.contentSize.width;}
                else {mySprite.scale =1;}
            } else {
                if (x_maxmenu/mySprite.contentSize.height<1) {mySprite.scale = (float)x_maxmenu/mySprite.contentSize.height;}
                else {mySprite.scale =1;}
            }
            [self addChild:mySprite z:311 tag:184+i];
            [movableelements addObject:mySprite];
            mySprite.visible=NO;
            [level_item addObject:[NSNumber numberWithInteger:170]];
            if ([Singleton sharedSingleron].Trial_Mode==YES) {
                if (i>2) {
                    CCSprite *zamok = [CCSprite spriteWithSpriteFrameName:@"zamok.png"];
                    zamok.position = ccp(mySprite.contentSize.width/2, mySprite.contentSize.height/2);
                    zamok.scale=1/mySprite.scale;
                    [mySprite addChild:zamok z:1 tag:10184+i];
                }
            }
        }
        
        for(int i = 0; i < 27; ++i) {
            NSString *image = [NSString stringWithFormat:@"hat%d.png", i+1];
            mySprite = [CCSprite spriteWithSpriteFrameName:image];
            mySprite.position = CGPointMake((float)(i+0.5)*x_maxmenu+(i+0.5)*x_between, panel_elements.contentSize.height/2);
            if (mySprite.contentSize.width>mySprite.contentSize.height) {
                if (x_maxmenu/mySprite.contentSize.width<1) {mySprite.scale = (float)x_maxmenu/mySprite.contentSize.width;}
                else {mySprite.scale =1;}
            } else {
                if (x_maxmenu/mySprite.contentSize.height<1) {mySprite.scale = (float)x_maxmenu/mySprite.contentSize.height;}
                else {mySprite.scale =1;}
            }
            [self addChild:mySprite z:311 tag:191+i];
            [movableelements addObject:mySprite];
            mySprite.visible=NO;
            [level_item addObject:[NSNumber numberWithInteger:180]];
            if ([Singleton sharedSingleron].Trial_Mode==YES) {
                if (i>2) {
                    CCSprite *zamok = [CCSprite spriteWithSpriteFrameName:@"zamok.png"];
                    zamok.position = ccp(mySprite.contentSize.width/2, mySprite.contentSize.height/2);
                    zamok.scale=1/mySprite.scale;
                    [mySprite addChild:zamok z:1 tag:10191+i];
                }
            }
        }
        
        for(int i = 0; i < 22; ++i) {
            NSString *image = [NSString stringWithFormat:@"jacket%d.png", i+1];
            mySprite = [CCSprite spriteWithSpriteFrameName:image];
            mySprite.position = CGPointMake((float)(i+0.5)*x_maxmenu+(i+0.5)*x_between, panel_elements.contentSize.height/2);
            if (mySprite.contentSize.width>mySprite.contentSize.height) {
                if (x_maxmenu/mySprite.contentSize.width<1) {mySprite.scale = (float)x_maxmenu/mySprite.contentSize.width;}
                else {mySprite.scale =1;}
            } else {
                if (x_maxmenu/mySprite.contentSize.height<1) {mySprite.scale = (float)x_maxmenu/mySprite.contentSize.height;}
                else {mySprite.scale =1;}
            }
            [self addChild:mySprite z:311 tag:218+i];
            [movableelements addObject:mySprite];
            mySprite.visible=NO;
            if ([Singleton sharedSingleron].Trial_Mode==YES) {
                if (i>2) {
                    CCSprite *zamok = [CCSprite spriteWithSpriteFrameName:@"zamok.png"];
                    zamok.position = ccp(mySprite.contentSize.width/2, mySprite.contentSize.height/2);
                    zamok.scale=1/mySprite.scale;
                    [mySprite addChild:zamok z:1 tag:10218+i];
                }
            }
            if (i==7) {[level_item addObject:[NSNumber numberWithInteger:162]];} else {[level_item addObject:[NSNumber numberWithInteger:100]];}
        }
        
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"obj2.plist"];
        [[CCTextureCache sharedTextureCache] addImage:@"obj2.pvr.ccz" ];
        
        for(int i = 0; i < 27; ++i) {
            NSString *image = [NSString stringWithFormat:@"top%d.png", i+1];
            mySprite = [CCSprite spriteWithSpriteFrameName:image];
            mySprite.position = CGPointMake((float)(i+0.5)*x_maxmenu+(i+0.5)*x_between, panel_elements.contentSize.height/2);
            if (mySprite.contentSize.width>mySprite.contentSize.height) {
                if (x_maxmenu/mySprite.contentSize.width<1) {mySprite.scale = (float)x_maxmenu/mySprite.contentSize.width;}
                else {mySprite.scale =1;}
            } else {
                if (x_maxmenu/mySprite.contentSize.height<1) {mySprite.scale = (float)x_maxmenu/mySprite.contentSize.height;}
                else {mySprite.scale =1;}
            }
            [self addChild:mySprite z:311 tag:240+i];
            [movableelements addObject:mySprite];
            mySprite.visible=NO;
            [level_item addObject:[NSNumber numberWithInteger:60]];
            if ([Singleton sharedSingleron].Trial_Mode==YES) {
                if (i>2) {
                    CCSprite *zamok = [CCSprite spriteWithSpriteFrameName:@"zamok.png"];
                    zamok.position = ccp(mySprite.contentSize.width/2, mySprite.contentSize.height/2);
                    zamok.scale=1/mySprite.scale;
                    [mySprite addChild:zamok z:1 tag:10240+i];
                }
            }
        }
        
        for(int i = 0; i < 18; ++i) {
            NSString *image = [NSString stringWithFormat:@"pants%d.png", i+1];
            mySprite = [CCSprite spriteWithSpriteFrameName:image];
            mySprite.position = CGPointMake((float)(i+0.5)*x_maxmenu+(i+0.5)*x_between, panel_elements.contentSize.height/2);
            if (mySprite.contentSize.width>mySprite.contentSize.height) {
                if (x_maxmenu/mySprite.contentSize.width<1) {mySprite.scale = (float)x_maxmenu/mySprite.contentSize.width;}
                else {mySprite.scale =1;}
            } else {
                if (x_maxmenu/mySprite.contentSize.height<1) {mySprite.scale = (float)x_maxmenu/mySprite.contentSize.height;}
                else {mySprite.scale =1;}
            }
            [self addChild:mySprite z:311 tag:267+i];
            [movableelements addObject:mySprite];
            mySprite.visible=NO;
            if ([Singleton sharedSingleron].Trial_Mode==YES) {
                if (i>2) {
                    CCSprite *zamok = [CCSprite spriteWithSpriteFrameName:@"zamok.png"];
                    zamok.position = ccp(mySprite.contentSize.width/2, mySprite.contentSize.height/2);
                    zamok.scale=1/mySprite.scale;
                    [mySprite addChild:zamok z:1 tag:10267+i];
                }
            }
            if ((i==2)||(i==3)||(i==4)||(i==6)) {[level_item addObject:[NSNumber numberWithInteger:19]];} else {[level_item addObject:[NSNumber numberWithInteger:50]];}
        }
        
        for(int i = 0; i < 22; ++i) {
            NSString *image = [NSString stringWithFormat:@"skirt%d.png", i+1];
            mySprite = [CCSprite spriteWithSpriteFrameName:image];
            mySprite.position = CGPointMake((float)(i+0.5)*x_maxmenu+(i+0.5)*x_between, panel_elements.contentSize.height/2);
            if (mySprite.contentSize.width>mySprite.contentSize.height) {
                if (x_maxmenu/mySprite.contentSize.width<1) {mySprite.scale = (float)x_maxmenu/mySprite.contentSize.width;}
                else {mySprite.scale =1;}
            } else {
                if (x_maxmenu/mySprite.contentSize.height<1) {mySprite.scale = (float)x_maxmenu/mySprite.contentSize.height;}
                else {mySprite.scale =1;}
            }
            [self addChild:mySprite z:311 tag:285+i];
            [movableelements addObject:mySprite];
            mySprite.visible=NO;
            //if ((i==0)||((i>9)&&(i<15))) {[level_item addObject:[NSNumber numberWithInteger:60]];} else {[level_item addObject:[NSNumber numberWithInteger:30]];} было закоментарено
            [level_item addObject:[NSNumber numberWithInteger:50]];
            if ([Singleton sharedSingleron].Trial_Mode==YES) {
                if (i>2) {
                    CCSprite *zamok = [CCSprite spriteWithSpriteFrameName:@"zamok.png"];
                    zamok.position = ccp(mySprite.contentSize.width/2, mySprite.contentSize.height/2);
                    zamok.scale=1/mySprite.scale;
                    [mySprite addChild:zamok z:1 tag:10285+i];
                }
            }
        }
        
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"obj3.plist"];
        [[CCTextureCache sharedTextureCache] addImage:@"obj3.pvr.ccz" ];
        
        for(int i = 0; i < 12; ++i) {
            NSString *image = [NSString stringWithFormat:@"dress%d.png", i+1];
            mySprite = [CCSprite spriteWithSpriteFrameName:image];
            mySprite.position = CGPointMake((float)(i+0.5)*x_maxmenu+(i+0.5)*x_between, panel_elements.contentSize.height/2);
            if (mySprite.contentSize.width>mySprite.contentSize.height) {
                if (x_maxmenu/mySprite.contentSize.width<1) {mySprite.scale = (float)x_maxmenu/mySprite.contentSize.width;}
                else {mySprite.scale =1;}
            } else {
                if (x_maxmenu/mySprite.contentSize.height<1) {mySprite.scale = (float)x_maxmenu/mySprite.contentSize.height;}
                else {mySprite.scale =1;}
            }
            [self addChild:mySprite z:311 tag:307+i];
            [movableelements addObject:mySprite];
            mySprite.visible=NO;
            [level_item addObject:[NSNumber numberWithInteger:70]];
            if ([Singleton sharedSingleron].Trial_Mode==YES) {
                if (i>2) {
                    CCSprite *zamok = [CCSprite spriteWithSpriteFrameName:@"zamok.png"];
                    zamok.position = ccp(mySprite.contentSize.width/2, mySprite.contentSize.height/2);
                    zamok.scale=1/mySprite.scale;
                    [mySprite addChild:zamok z:1 tag:10307+i];
                }
            }
        }
        
        for(int i = 0; i < 10; ++i) {
            NSString *image = [NSString stringWithFormat:@"stockings%d.png", i+1];
            mySprite = [CCSprite spriteWithSpriteFrameName:image];
            mySprite.position = CGPointMake((float)(i+0.5)*x_maxmenu+(i+0.5)*x_between, panel_elements.contentSize.height/2);
            if (mySprite.contentSize.width>mySprite.contentSize.height) {
                if (x_maxmenu/mySprite.contentSize.width<1) {mySprite.scale = (float)x_maxmenu/mySprite.contentSize.width;}
                else {mySprite.scale =1;}
            } else {
                if (x_maxmenu/mySprite.contentSize.height<1) {mySprite.scale = (float)x_maxmenu/mySprite.contentSize.height;}
                else {mySprite.scale =1;}
            }
            [self addChild:mySprite z:311 tag:319+i];
            [movableelements addObject:mySprite];
            mySprite.visible=NO;
            [level_item addObject:[NSNumber numberWithInteger:10]];
            if ([Singleton sharedSingleron].Trial_Mode==YES) {
                if (i>2) {
                    CCSprite *zamok = [CCSprite spriteWithSpriteFrameName:@"zamok.png"];
                    zamok.position = ccp(mySprite.contentSize.width/2, mySprite.contentSize.height/2);
                    zamok.scale=1/mySprite.scale;
                    [mySprite addChild:zamok z:1 tag:10319+i];
                }
            }
        }
        
        for(int i = 0; i < 20; ++i) {
            NSString *image = [NSString stringWithFormat:@"shoes%d.png", i+1];
            mySprite = [CCSprite spriteWithSpriteFrameName:image];
            mySprite.position = CGPointMake((float)(i+0.5)*x_maxmenu+(i+0.5)*x_between, panel_elements.contentSize.height/2);
            if (mySprite.contentSize.width>mySprite.contentSize.height) {
                if (x_maxmenu/mySprite.contentSize.width<1) {mySprite.scale = (float)x_maxmenu/mySprite.contentSize.width;}
                else {mySprite.scale =1;}
            } else {
                if (x_maxmenu/mySprite.contentSize.height<1) {mySprite.scale = (float)x_maxmenu/mySprite.contentSize.height;}
                else {mySprite.scale =1;}
            }
            [self addChild:mySprite z:311 tag:329+i];
            [movableelements addObject:mySprite];
            mySprite.visible=NO;
            [level_item addObject:[NSNumber numberWithInteger:20]];
            if ([Singleton sharedSingleron].Trial_Mode==YES) {
                if (i>2) {
                    CCSprite *zamok = [CCSprite spriteWithSpriteFrameName:@"zamok.png"];
                    zamok.position = ccp(mySprite.contentSize.width/2, mySprite.contentSize.height/2);
                    zamok.scale=1/mySprite.scale;
                    [mySprite addChild:zamok z:1 tag:10329+i];
                }
            }
        }
        
        for(int i = 0; i < 8; ++i) {
            mySprite = [CCSprite spriteWithSpriteFrameName:@"tochka.png"];
            mySprite.position = CGPointMake(i*8, 8);
            mySprite.scale=0.5;
            mySprite.color=ccBLACK;
            [self addChild:mySprite z:311 tag:9001+i];
            mySprite.visible=NO;
        }
        
        //[[CCSpriteFrameCache sharedSpriteFrameCache] removeUnusedSpriteFrames];

        first_item = [[NSMutableArray alloc] init];
        [first_item addObject:[NSNumber numberWithInteger:100]];
        [first_item addObject:[NSNumber numberWithInteger:112]];
        [first_item addObject:[NSNumber numberWithInteger:129]];
        [first_item addObject:[NSNumber numberWithInteger:137]];
        [first_item addObject:[NSNumber numberWithInteger:151]];
        [first_item addObject:[NSNumber numberWithInteger:183]];
        [first_item addObject:[NSNumber numberWithInteger:190]];
        [first_item addObject:[NSNumber numberWithInteger:217]];
        [first_item addObject:[NSNumber numberWithInteger:239]];
        [first_item addObject:[NSNumber numberWithInteger:266]];
        [first_item addObject:[NSNumber numberWithInteger:284]];
        [first_item addObject:[NSNumber numberWithInteger:306]];
        [first_item addObject:[NSNumber numberWithInteger:318]];
        [first_item addObject:[NSNumber numberWithInteger:328]];
        
        //InApp
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productsLoaded:) name:kProductsLoadedNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:kProductPurchasedNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(productPurchaseFailed:) name:kProductPurchaseFailedNotification object: nil];
        
        can_back_change=YES;
        active_category=1;
        active_elements=-1;
        [self select_category];
        button_can_touch=YES;
        time_for_flurry=0;
	}
    
	return self;
}

-(void) gobackscene {
    //maindialog.position = ccp(-winsize.width/2, -winsize.height/2);
    [[SimpleAudioEngine sharedEngine] playEffect:@"button4-1.caf"];
    [Singleton sharedSingleron].rerun_choise=YES;
    [[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:1 scene:[ChoiceModel scene]]];
}

-(void) changebackground{
    //maindialog.position = ccp(-winsize.width/2, -winsize.height/2);
    if (can_back_change==YES) {
        [[SimpleAudioEngine sharedEngine] playEffect:@"button4-1.caf"];
        can_back_change=NO;
        CCSprite *temp_background1 = (CCSprite *)[self getChildByTag:1];
        CCSprite *temp_background2 = (CCSprite *)[self getChildByTag:2];
        CCSprite *temp_background3 = (CCSprite *)[self getChildByTag:3];
        if (temp_background1.opacity==255) {
            [self reorderChild:temp_background2 z:2];
            [temp_background2 runAction:[CCSequence actions:[CCFadeIn actionWithDuration:1], [CCCallFunc actionWithTarget:self selector:@selector(stopchangeBackground1)], nil]];
        }
        if (temp_background2.opacity==255) {
            [self reorderChild:temp_background3 z:2];
            [temp_background3 runAction:[CCSequence actions:[CCFadeIn actionWithDuration:1], [CCCallFunc actionWithTarget:self selector:@selector(stopchangeBackground2)], nil]];
        }
        if (temp_background3.opacity==255) {
            [self reorderChild:temp_background1 z:2];
            [temp_background1 runAction:[CCSequence actions:[CCFadeIn actionWithDuration:1], [CCCallFunc actionWithTarget:self selector:@selector(stopchangeBackground3)], nil]];
        }
    }
}

-(void) stopchangeBackground1{
    CCSprite *temp_background1 = (CCSprite *)[self getChildByTag:1];
    CCSprite *temp_background2 = (CCSprite *)[self getChildByTag:2];
    [self reorderChild:temp_background2 z:1];
    CCSprite * temp_dim = (CCSprite *)[temp_background2 getChildByTag:7];
    CCAction *actionfade = [CCFadeIn actionWithDuration:1];
    [actionfade setTag:6];
    [temp_dim runAction:actionfade];
    temp_background1.opacity=0;
    can_back_change=YES;
}

-(void) stopchangeBackground2{
    CCSprite *temp_background2 = (CCSprite *)[self getChildByTag:2];
    CCSprite * temp_dim = (CCSprite *)[temp_background2 getChildByTag:7];
    [temp_dim stopActionByTag:6];
    temp_dim.opacity=0;
    CCSprite *temp_background3 = (CCSprite *)[self getChildByTag:3];
    [self reorderChild:temp_background3 z:1];
    temp_background2.opacity=0;
    can_back_change=YES;
}

-(void) stopchangeBackground3{
    CCSprite *temp_background1 = (CCSprite *)[self getChildByTag:1];
    CCSprite *temp_background3 = (CCSprite *)[self getChildByTag:3];
    [self reorderChild:temp_background1 z:1];
    temp_background3.opacity=0;
    can_back_change=YES;
}

-(void) sharephoto{
    if (dialog1==NO) {
        [[SimpleAudioEngine sharedEngine] playEffect:@"button4-1.caf"];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                            message:NSLocalizedString(@"", nil)
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                                  otherButtonTitles:NSLocalizedString(@"Facebook", nil), NSLocalizedString(@"Email", nil), nil];
        [alertView show];
        [alertView setTag:77777];
        dialog1=YES;
    }
    //maindialog.position = ccp(winsize.width/2, winsize.height/2);
}

-(void)sendmail {
    [[SimpleAudioEngine sharedEngine] playEffect:@"button4-1.caf"];
    //maindialog.position = ccp(-winsize.width/2, -winsize.height/2);
    for (CCSprite *mySprite1 in movableelements) {
        if (((active_category<14)&&(mySprite1.tag>[[first_item objectAtIndex:active_category-1] integerValue])&&(mySprite1.tag<[[first_item objectAtIndex:active_category] integerValue]+1))||((active_category==14)&&(mySprite1.tag>[[first_item objectAtIndex:active_category-1] integerValue]))) {
            mySprite1.visible=NO;
        }
    }
    for (CCSprite *mySprite1 in movablebutton) {
        mySprite1.visible=NO;
    }
    for (CCMenuItemFont *each in [mainmenu children]) {
        each.visible=NO;
    }
    [self getChildByTag:5].visible=NO;
    MailSender *mailSender = [MailSender alloc];
    //UIImage *image = [[UIImage alloc] init];
    //image = [CCScreenshot screenshotWithStartNode:self];
    [mailSender openMail:[CCScreenshot screenshotWithStartNode:self]];
    [self getChildByTag:5].visible=YES;
    for (CCMenuItemFont *each in [mainmenu children]) {
        each.visible=YES;
    }
    for (CCSprite *mySprite1 in movablebutton) {
        mySprite1.visible=YES;
    }
    for (CCSprite *mySprite1 in movableelements) {
        if (((active_category<14)&&(mySprite1.tag>[[first_item objectAtIndex:active_category-1] integerValue])&&(mySprite1.tag<[[first_item objectAtIndex:active_category] integerValue]+1))||((active_category==14)&&(mySprite1.tag>[[first_item objectAtIndex:active_category-1] integerValue]))) {
            mySprite1.visible=YES;
        }
    }
    //image=nil;
    //[image release];
}

-(void) captureToPhotoAlbum{
    //if (maindialog.position.x==-winsize.width/2) {
        for (CCSprite *mySprite1 in movableelements) {
            if (((active_category<14)&&(mySprite1.tag>[[first_item objectAtIndex:active_category-1] integerValue])&&(mySprite1.tag<[[first_item objectAtIndex:active_category] integerValue]+1))||((active_category==14)&&(mySprite1.tag>[[first_item objectAtIndex:active_category-1] integerValue]))) {
                mySprite1.visible=NO;
            }
        }
        for (CCSprite *mySprite1 in movablebutton) {
            mySprite1.visible=NO;
        }
        for (CCMenuItemFont *each in [mainmenu children]) {
            each.visible=NO;
        }
        [self getChildByTag:5].visible=NO;
        [[SimpleAudioEngine sharedEngine] playEffect:@"photo.caf"];
        //UIImage *image = [[UIImage alloc] init];
        //image = [CCScreenshot screenshotWithStartNode:self];
        UIImageWriteToSavedPhotosAlbum([CCScreenshot screenshotWithStartNode:self], self, nil, nil);
        [self getChildByTag:5].visible=YES;
        for (CCMenuItemFont *each in [mainmenu children]) {
            each.visible=YES;
        }
        for (CCSprite *mySprite1 in movablebutton) {
            mySprite1.visible=YES;
        }
        for (CCSprite *mySprite1 in movableelements) {
            if (((active_category<14)&&(mySprite1.tag>[[first_item objectAtIndex:active_category-1] integerValue])&&(mySprite1.tag<[[first_item objectAtIndex:active_category] integerValue]+1))||((active_category==14)&&(mySprite1.tag>[[first_item objectAtIndex:active_category-1] integerValue]))) {
                mySprite1.visible=YES;
            }
        }
        //image=nil;
        //[image release];
   // }
}

-(void) resetgame{
    //if (maindialog.position.x==-winsize.width/2) {
        [[SimpleAudioEngine sharedEngine] playEffect:@"button4-1.caf"];
        int i=1;
        for (CCSprite *mySprite1 in movableelements) {
            if (mySprite1.opacity<200) {
                mySprite1.opacity=255;
                CCSprite *sprite_for_delete = (CCSprite *) [self getChildByTag:500+i];
                [self removeChild:sprite_for_delete cleanup:YES];
            }
            ++i;
        }
        [self getChildByTag:8].visible=NO;
    //}
}

-(void) panelofbuttonmoved :(CGPoint)translation {
    for (CCSprite *each in movablebutton) {
        each.position = CGPointMake(each.position.x, each.position.y+translation.y);;
    }
}

-(void) reversepanelbuttonwithvelocity:(CGPoint)velocity_user {
    button_can_touch=NO;
    float min_y_panel = 100000;
    float max_y_panel = -100000;
    int i=0;
    
    for (CCSprite *mySprite1 in movablebutton) {
        if (min_y_panel>mySprite1.position.y) {min_y_panel=mySprite1.position.y;}
        if (max_y_panel<mySprite1.position.y) {max_y_panel=mySprite1.position.y;}
    }
    
    i=0;
    float start_y;
    if (max_y_panel+velocity_user.y<(winsize.height-2*between_button_y-size_button/2)) {
        for (CCSprite *mySprite1 in movablebutton) {
            [mySprite1 stopAllActions];
            [mySprite1 runAction:[CCSequence actions:[CCEaseExponentialOut actionWithAction:[CCMoveTo actionWithDuration:0.5 position:ccp(mySprite1.position.x, winsize.height-2*between_button_y-size_button/2-i*(between_button_y+size_button))]], [CCCallFunc actionWithTarget:self selector:@selector(buttoncantouch)], nil]];
            ++i;
        }
    } else {
        if (min_y_panel+velocity_user.y>[self getChildByTag:5].contentSize.height) {
            int count_now=14;
            start_y=[self getChildByTag:5].contentSize.height+between_button_y+count_now*(between_button_y+size_button);
            for (CCSprite *mySprite1 in movablebutton) {
                [mySprite1 stopAllActions];
                [mySprite1 runAction:[CCSequence actions:[CCEaseExponentialOut actionWithAction:[CCMoveTo actionWithDuration:0.5 position:ccp(mySprite1.position.x, start_y-2*between_button_y-size_button/2-i*(between_button_y+size_button))]], [CCCallFunc actionWithTarget:self selector:@selector(buttoncantouch)], nil]];
                ++i;
            }
        } else {
            for (CCSprite *mySprite1 in movablebutton) {
                [mySprite1 stopAllActions];
                [mySprite1 runAction:[CCSequence actions:[CCEaseExponentialOut actionWithAction:[CCMoveTo actionWithDuration:0.5 position:ccp(mySprite1.position.x, mySprite1.position.y+velocity_user.y)]], [CCCallFunc actionWithTarget:self selector:@selector(buttoncantouch)], nil]];
                ++i;
            }
        }
    }
}

-(void) buttoncantouch {
    button_can_touch=YES;
}

-(void) sendfacebook  {
    //maindialog.position = ccp(-winsize.width/2, -winsize.height/2);
    [[SimpleAudioEngine sharedEngine] playEffect:@"button4-1.caf"];
    
    if (facebook == nil) {
		facebook = [[Facebook alloc] initWithAppId:@"515194525180815"];
	}
	
	NSArray* permissions =  [[NSArray arrayWithObjects:
							  @"publish_stream", @"offline_access", nil] retain];
	
	[facebook authorize:permissions delegate:self];
}

- (BOOL) takeScreenshot
{
	for (CCSprite *mySprite1 in movableelements) {
        if (((active_category<14)&&(mySprite1.tag>[[first_item objectAtIndex:active_category-1] integerValue])&&(mySprite1.tag<[[first_item objectAtIndex:active_category] integerValue]+1))||((active_category==14)&&(mySprite1.tag>[[first_item objectAtIndex:active_category-1] integerValue]))) {
            mySprite1.visible=NO;
        }
    }
    for (CCSprite *mySprite1 in movablebutton) {
        mySprite1.visible=NO;
    }
    for (CCMenuItemFont *each in [mainmenu children]) {
        each.visible=NO;
    }
    [self getChildByTag:5].visible=NO;
    //UIImage *tempImage = [[UIImage alloc] init];
    //tempImage = [CCScreenshot screenshotWithStartNode:self];
    
	NSMutableDictionary *params = [NSMutableDictionary dictionary];
    //[params setObject:myCaption forKey:@"caption"];
    [params setObject:NSLocalizedString(@"I've just created a new look on my iPhone with Walks in Paris!", nil) forKey:@"message"];
    // UIImage object goes here
    [params setObject:[CCScreenshot screenshotWithStartNode:self] forKey:@"picture"];
	[facebook requestWithGraphPath:@"me/photos" andParams:params andHttpMethod:@"POST" andDelegate:self];
	
    [self getChildByTag:5].visible=YES;
    for (CCMenuItemFont *each in [mainmenu children]) {
        each.visible=YES;
    }
    for (CCSprite *mySprite1 in movablebutton) {
        mySprite1.visible=YES;
    }
    for (CCSprite *mySprite1 in movableelements) {
        if (((active_category<14)&&(mySprite1.tag>[[first_item objectAtIndex:active_category-1] integerValue])&&(mySprite1.tag<[[first_item objectAtIndex:active_category] integerValue]+1))||((active_category==14)&&(mySprite1.tag>[[first_item objectAtIndex:active_category-1] integerValue]))) {
            mySprite1.visible=YES;
        }
    }
    
    alert_wait = [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Please wait...\nUploading screenshot.", nil) message:nil delegate:self cancelButtonTitle:nil otherButtonTitles: nil] autorelease];
    [alert_wait show];
    
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    // Adjust the indicator so it is up a few pixels from the bottom of the alert
    indicator.center = CGPointMake(alert_wait.bounds.size.width / 2, alert_wait.bounds.size.height - 50);
    [indicator startAnimating];
    [alert_wait addSubview:indicator];
    [indicator release];
    
    [params removeAllObjects];
	return YES;
}

/**
 * Called when the user canceled the authorization dialog.
 */
-(void)fbDidNotLogin:(BOOL)cancelled {
    [alert_wait dismissWithClickedButtonIndex:0 animated:YES];
	if (cancelled) {
        
	} else {
        UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Oops", nil)
                                                         message:NSLocalizedString(@"The photo could not be shared\non Facebook at the moment.", nil)
                                                        delegate:nil
                                               cancelButtonTitle:nil
                                               otherButtonTitles:@"OK", nil] autorelease];
        
        [alert show];
	}
}

/**
 * Called when the request logout has succeeded.
 */
- (void)fbDidLogout {
	isFBLogged = NO;
	facebookLoginButton.visible = YES;
	facebookLogoutButton.visible = NO;
}

#pragma mark -
#pragma mark FBRequestDelegate
/**
 * Called when the Facebook API request has returned a response. This callback
 * gives you access to the raw response. It's called before
 * (void)request:(FBRequest *)request didLoad:(id)result,
 * which is passed the parsed response object.
 */
- (void)request:(FBRequest *)request didReceiveResponse:(NSURLResponse *)response {
	
}

//отправка на фэйсбук успешно завершена
- (void)request:(FBRequest *)request didLoad:(id)result {
    [alert_wait dismissWithClickedButtonIndex:0 animated:YES];
    UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Success!", nil)
                                                     message:NSLocalizedString(@"Your photo has been uploaded\nto Facebook", nil)
                                                    delegate:nil
                                           cancelButtonTitle:nil
                                           otherButtonTitles:@"OK", nil] autorelease];
    
    [alert show];
};

//отправка на фэйсбук завершена с ошибкой
- (void)request:(FBRequest *)request didFailWithError:(NSError *)error {
    [alert_wait dismissWithClickedButtonIndex:0 animated:YES];
    UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Oops", nil)
                                                     message:NSLocalizedString(@"The photo could not be shared\non Facebook at the moment.", nil)
                                                    delegate:nil
                                           cancelButtonTitle:nil
                                           otherButtonTitles:@"OK", nil] autorelease];
    
    [alert show];
};

#pragma mark -
#pragma mark FBDialogDelegate

//отправка на фэйсбук успешно завершена
- (void)dialogDidComplete:(FBDialog *)dialog {
    [alert_wait dismissWithClickedButtonIndex:0 animated:YES];
    UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Success!", nil)
                                                     message:NSLocalizedString(@"Your photo has been uploaded\nto Facebook", nil)
                                                    delegate:nil
                                           cancelButtonTitle:nil
                                           otherButtonTitles:@"OK", nil] autorelease];
    
    [alert show];
}

-(void) select_category {
    int i=1;
    for (CCSprite *each in movablebutton) {
        CCSprite *temp =(CCSprite *) [each getChildByTag:50+i];
        temp.opacity = 0;
        ++i;
    }
    for (CCSprite *each in movablebutton) {
        if (each.tag==active_category+10) {
            CCSprite *temp =(CCSprite *) [each getChildByTag:50+active_category];
            temp.opacity = 255;
        }
    }
    i=1;
    int first=0;
    for (CCSprite *mySprite1 in movableelements) {
        if (((active_category<14)&&(mySprite1.tag>[[first_item objectAtIndex:active_category-1] integerValue])&&(mySprite1.tag<[[first_item objectAtIndex:active_category] integerValue]+1))||((active_category==14)&&(mySprite1.tag>[[first_item objectAtIndex:active_category-1] integerValue]))) {
            mySprite1.visible=YES;
            if ((first==0)&&(mySprite1.position.x>0)) {
                first=i;
            }
            ++i;
        } else {
            mySprite1.visible=NO;
        }
    }
    
    int count_pages;
    if (active_category!=14) {
        if (([[first_item objectAtIndex:active_category] integerValue]-[[first_item objectAtIndex:active_category-1] integerValue])%4==0) {
            count_pages=([[first_item objectAtIndex:active_category] integerValue]-[[first_item objectAtIndex:active_category-1] integerValue])/4;
        } else {
            count_pages=([[first_item objectAtIndex:active_category] integerValue]-[[first_item objectAtIndex:active_category-1] integerValue]-([[first_item objectAtIndex:active_category] integerValue]-[[first_item objectAtIndex:active_category-1] integerValue])%4)/4+1;
        }
    } else {
       count_pages=5;
    }
    
    float first_x_tochka;
    if (count_pages%2==0) {
        first_x_tochka=winsize.width/2-4-8*(count_pages/2-1);
    } else {
        first_x_tochka=winsize.width/2-8*((count_pages-1)/2);
    }
    int active_page;
    if (first%4==0) {
        active_page=first/4;
    } else {
        active_page=(first-first%4)/4+1;
    }
    for(int i = 0; i < 8; ++i) {
        if (i>count_pages-1) {
            [self getChildByTag:9001+i].visible=NO;
        } else {
            CCSprite *temp = (CCSprite *) [self getChildByTag:9001+i];
            temp.visible=YES;
            temp.position=ccp(first_x_tochka+i*8, 8);
            if (active_page==i+1) {
                temp.color=ccWHITE;
            } else {
                temp.color=ccBLACK;
            }
        }
    }
}

-(void) verify_obj {
    int number_in_category=100+active_elements-[[first_item objectAtIndex:active_category-1] integerValue];
    //NSLog(@"number_in_category=%d",number_in_category);
    int i=1;
    for (CCSprite *mySprite1 in movableelements) {
        if (mySprite1.opacity<200) {
            int temp_category=14;
            for (int j = 1; j < 13; ++j) {
                if ((mySprite1.tag>[[first_item objectAtIndex:j-1] integerValue])&&(mySprite1.tag<[[first_item objectAtIndex:j] integerValue]+1)) {
                    temp_category=j;
                    break;
                }
            }
            int change_obj=0;
            if (((active_category<14)&&(mySprite1.tag>[[first_item objectAtIndex:active_category-1] integerValue])&&(mySprite1.tag<[[first_item objectAtIndex:active_category] integerValue]+1))||((active_category==14)&&(mySprite1.tag>[[first_item objectAtIndex:active_category-1] integerValue]))) {
                int temp_number_in_category=100+i-[[first_item objectAtIndex:active_category-1] integerValue];
                change_obj=1; // если 1 то будет замена
                if (active_category==4) {
                    //драгоценности
                    change_obj=0;
                    if ((temp_number_in_category<5)&&(number_in_category<5)) {change_obj=1;}
                    if (((temp_number_in_category>4)&&(temp_number_in_category<9))&&((number_in_category>4)&&(number_in_category<9))) {change_obj=1;}
                    if (((temp_number_in_category==10)||(temp_number_in_category==11))&&((number_in_category==10)||(number_in_category==11))) {change_obj=1;}
                    if (((temp_number_in_category==9)||(temp_number_in_category==12))&&((number_in_category==9)||(number_in_category==12))) {change_obj=1;}
                }
                if (active_category==5) {
                    //проверка челки
                    if (((temp_number_in_category!=4)&&(temp_number_in_category<11))&&(number_in_category==2)) {change_obj=0;}
                    if (((temp_number_in_category!=15)&&((temp_number_in_category>10)||(temp_number_in_category<22)))&&(number_in_category==13)) {change_obj=0;}
                    if (((temp_number_in_category!=26)&&(temp_number_in_category>21))&&(number_in_category==24)) {change_obj=0;}
                    if (((temp_number_in_category==2))&&((number_in_category!=4)&&(number_in_category<11))) {change_obj=0;}
                    if (((temp_number_in_category==13))&&((number_in_category!=15)&&((number_in_category>10)||(number_in_category<22)))) {change_obj=0;}
                    if (((temp_number_in_category==24))&&((number_in_category!=26)&&(number_in_category>21))) {change_obj=0;}
                    if (((number_in_category==2)||(number_in_category==13)||(number_in_category==26))&&((temp_number_in_category==2)||(temp_number_in_category==13)||(temp_number_in_category==26))) {change_obj=1;}
                    //волосы разного цвета
                    if ((temp_number_in_category<11)&&(number_in_category>10)) {change_obj=1;}
                    if (((temp_number_in_category>10)&&(temp_number_in_category<22))&&((number_in_category<11)||(number_in_category>21))) {change_obj=1;}
                    if ((temp_number_in_category>21)&&(number_in_category<22)) {change_obj=1;}
                    
                }
            } else {
                // замена юбки со штанами
                if ((temp_category==10)&&(active_category==11)) {change_obj=1;}
                if ((temp_category==11)&&(active_category==10)) {change_obj=1;}
                // платья с юбками
                if ((temp_category==12)&&(active_category==11)) {change_obj=1;}
                if ((temp_category==11)&&(active_category==12)) {change_obj=1;}
                // платья с рубашками
                if ((temp_category==12)&&(active_category==9)) {change_obj=1;}
                if ((temp_category==9)&&(active_category==12)) {change_obj=1;}
                // платья с брюками
                if ((temp_category==12)&&(active_category==10)) {change_obj=1;}
                if ((temp_category==10)&&(active_category==12)) {change_obj=1;}
            }
            if (change_obj==1) {
                mySprite1.opacity=255;
                CCSprite *sprite_for_delete = (CCSprite *) [self getChildByTag:500+i];
                [self removeChild:sprite_for_delete cleanup:YES];
            }
        }
        ++i;
   
    }
    CCSprite *temp =(CCSprite *) [self getChildByTag:717];
    if (temp!=nil) {
        [self getChildByTag:8].visible=YES;
    } else {
        temp =(CCSprite *) [self getChildByTag:718];
        if (temp!=nil) {
            [self getChildByTag:8].visible=YES;
        } else {
            [self getChildByTag:8].visible=NO;
        }
    }
}

-(void) select_elements {
    [[SimpleAudioEngine sharedEngine] playEffect:@"vzhik-1.caf"];
    CCSprite *sprite_for_delete = (CCSprite *) [self getChildByTag:500+active_elements];
    if (sprite_for_delete!=nil) {
        for (CCSprite *mySprite1 in movableelements) {
            if (mySprite1.tag==100+active_elements) {
                mySprite1.opacity=255;
            }
        }
        [self removeChild:sprite_for_delete cleanup:YES];
    } else {
        [self verify_obj];
        
        CGPoint gamepoint[] = {
            //1
            CGPointMake(158,369),
            CGPointMake(158,370),
            CGPointMake(158,369),
            CGPointMake(154,367),
            CGPointMake(154,367),
            CGPointMake(153,388),
            CGPointMake(153,388),
            CGPointMake(154,367),
            CGPointMake(154,367),
            CGPointMake(158,369),
            CGPointMake(158,369),
            CGPointMake(158,369),
            //2
            CGPointMake(148,314),
            CGPointMake(148,314),
            CGPointMake(148,314),
            CGPointMake(148,314),
            CGPointMake(154,310),
            CGPointMake(164,337),
            CGPointMake(164,337),
            CGPointMake(148,329),
            CGPointMake(145,324),
            CGPointMake(145,324),
            CGPointMake(197,236),
            CGPointMake(197,236),
            CGPointMake(199,231),
            CGPointMake(199,231),
            CGPointMake(194,225),
            CGPointMake(194,225),
            CGPointMake(203,211),
            //3
            CGPointMake(156,329),
            CGPointMake(158,323),
            CGPointMake(155,310),
            CGPointMake(155,311),
            CGPointMake(155,311),
            CGPointMake(155,311),
            CGPointMake(156.4,330),
            CGPointMake(156.4,330),
            //4
            CGPointMake(158,360),
            CGPointMake(155,357.6),
            CGPointMake(155,357.6),
            CGPointMake(153.4,384.6),
            CGPointMake(152.5,415),
            CGPointMake(152.5,416.6),
            CGPointMake(153,420),
            CGPointMake(153,420),
            CGPointMake(199,297),
            CGPointMake(115,321),
            CGPointMake(115,321),
            CGPointMake(200,287),
            CGPointMake(140,326),
            CGPointMake(140,326),
            //5
            CGPointMake(149,398),
            CGPointMake(152,443),
            CGPointMake(152,432),
            CGPointMake(152,427),
            CGPointMake(152,434),
            CGPointMake(153,426),
            CGPointMake(152,413),
            CGPointMake(152,398),
            CGPointMake(161,406),
            CGPointMake(153,446),
            CGPointMake(153,443),
            CGPointMake(149,398),
            CGPointMake(152,443),
            CGPointMake(152,432),
            CGPointMake(152,427),
            CGPointMake(152,434),
            CGPointMake(153,426),
            CGPointMake(152,413),
            CGPointMake(152,398),
            CGPointMake(161,406),
            CGPointMake(153,446),
            CGPointMake(153,443),
            CGPointMake(149,398),
            CGPointMake(152,443),
            CGPointMake(152,432),
            CGPointMake(152,427),
            CGPointMake(152,434),
            CGPointMake(153,426),
            CGPointMake(152,413),
            CGPointMake(152,398),
            CGPointMake(161,406),
            CGPointMake(153,446),
            //6
            CGPointMake(153,426),
            CGPointMake(153,426),
            CGPointMake(153,428),
            CGPointMake(152.4,428),
            CGPointMake(153,426),
            CGPointMake(153,426),
            CGPointMake(153,426),
            //7
            CGPointMake(153,444),
            CGPointMake(153,444),
            CGPointMake(153,443),
            CGPointMake(152.5,437),
            CGPointMake(152.5,437),
            CGPointMake(152.5,437),
            CGPointMake(152.5,449),
            CGPointMake(152.5,449),
            CGPointMake(152.5,449),
            CGPointMake(157,443),
            CGPointMake(157,443),
            CGPointMake(157,443),
            CGPointMake(157,443),
            CGPointMake(151,443),
            CGPointMake(151,443),
            CGPointMake(152,445),
            CGPointMake(152,445),
            CGPointMake(153,443),
            CGPointMake(153,443),
            CGPointMake(153,443),
            CGPointMake(153,443),
            CGPointMake(153,440),
            CGPointMake(153,440),
            CGPointMake(153,440),
            CGPointMake(153,440),
            CGPointMake(151,445),
            CGPointMake(151,445),
            //8
            CGPointMake(139,355),
            CGPointMake(141,347),
            CGPointMake(141,317.5),
            CGPointMake(141.5,327),
            CGPointMake(141,353.5),
            CGPointMake(141.2,353.8),
            CGPointMake(141.2,353.8),
            CGPointMake(143,356),
            CGPointMake(142,327.8),
            CGPointMake(142,329.8),
            CGPointMake(142,329.8),
            CGPointMake(141.2,346),
            CGPointMake(141.3,341.6),
            CGPointMake(141.3,341.6),
            CGPointMake(153.7,352.3),
            CGPointMake(153.7,352.3),
            CGPointMake(141.8,346),
            CGPointMake(141.8,346),
            CGPointMake(141.8,346),
            CGPointMake(139,324.5),
            CGPointMake(139,324.5),
            CGPointMake(141.2,330.6),
            //9
            CGPointMake(139.2,353.6),
            CGPointMake(140.4,350),
            CGPointMake(150.7,354.6),
            CGPointMake(141.9,353.6),
            CGPointMake(140,347),
            CGPointMake(140.2,349.4),
            CGPointMake(140.2,349.4),
            CGPointMake(140,347),
            CGPointMake(140,347),
            CGPointMake(139,343.5),
            CGPointMake(139,343.5),
            CGPointMake(140,349),
            CGPointMake(140,349),
            CGPointMake(142.2,357.2),
            CGPointMake(142,353.6),
            CGPointMake(140,347),
            CGPointMake(140,347),
            CGPointMake(156,354),
            CGPointMake(156,354),
            CGPointMake(156,354),
            CGPointMake(156,354),
            CGPointMake(156,354),
            CGPointMake(156,354),
            CGPointMake(156,354),
            CGPointMake(156,354),
            CGPointMake(141.2,345.6),
            CGPointMake(141.2,345.6),
            //10
            CGPointMake(171.7,231),
            CGPointMake(177,212.8),
            CGPointMake(172.7,224),
            CGPointMake(172.7,224),
            CGPointMake(172.7,224),
            CGPointMake(176.3,207.7),
            CGPointMake(169,236),
            CGPointMake(171,228),
            CGPointMake(171,228),
            CGPointMake(177.1,212.6),
            CGPointMake(177.1,212.6),
            CGPointMake(157,291),
            CGPointMake(157,292),
            CGPointMake(157,291),
            CGPointMake(157.2,291.6),
            CGPointMake(158,292),
            CGPointMake(158,292),
            CGPointMake(158,292),
            //11
            CGPointMake(157.5,287),
            CGPointMake(158.9,291),
            CGPointMake(158,284),
            CGPointMake(158,284),
            CGPointMake(159,289),
            CGPointMake(159,289),
            CGPointMake(159,289),
            CGPointMake(159,289),
            CGPointMake(159,289),
            CGPointMake(159,289),
            CGPointMake(169.3,257),
            CGPointMake(169.3,257),
            CGPointMake(163.6,228),
            CGPointMake(163.6,228),
            CGPointMake(159.6,280),
            CGPointMake(159.6,280),
            CGPointMake(159.6,280),
            CGPointMake(158,283),
            CGPointMake(158,283),
            CGPointMake(157.3,289),
            CGPointMake(163,262),
            CGPointMake(163,262),
            //12
            CGPointMake(157.6,308),
            CGPointMake(153,320),
            CGPointMake(159.2,322.8),
            CGPointMake(159.6,310),
            CGPointMake(160,308),
            CGPointMake(160,308),
            CGPointMake(139,321.6),
            CGPointMake(139.5,310),
            CGPointMake(161,304),
            CGPointMake(161,304),
            CGPointMake(167,240),
            CGPointMake(167,240),
            //13
            CGPointMake(179.5,211.5),
            CGPointMake(179.5,211.5),
            CGPointMake(179.5,211.5),
            CGPointMake(179.5,211.5),
            CGPointMake(179.5,211.5),
            CGPointMake(179.5,211.5),
            CGPointMake(182.7,170),
            CGPointMake(182.7,170),
            CGPointMake(183.2,166.4),
            CGPointMake(182.4,170),
            //14
            CGPointMake(185.5,114.5),
            CGPointMake(185,122),
            CGPointMake(185,137),
            CGPointMake(185,119),
            CGPointMake(185,119),
            CGPointMake(183.3,148),
            CGPointMake(183.3,148),
            CGPointMake(183.3,148),
            CGPointMake(185.5,114.5),
            CGPointMake(185.5,114.5),
            CGPointMake(185.5,114.5),
            CGPointMake(185,128),
            CGPointMake(185,128),
            CGPointMake(185.5,122),
            CGPointMake(185.5,122),
            CGPointMake(185.5,114.5),
            CGPointMake(185.5,114.5),
            CGPointMake(185.5,114.5),
            CGPointMake(187.5,114.5),
            CGPointMake(187.5,114.5)
        };
        
        NSString *image;
        int i=0;
        int j=0;
        for (CCSprite *mySprite1 in movableelements) {
            if (active_category<14) {
                if ((mySprite1.tag>[[first_item objectAtIndex:active_category-1] integerValue])&&(mySprite1.tag<[[first_item objectAtIndex:active_category] integerValue]+1)) {
                    if (mySprite1.tag==100+active_elements) {
                        switch (active_category) {
                            case 1:
                                image = [NSString stringWithFormat:@"scarf%d.png", j+1];
                                break;
                            case 2:
                                image = [NSString stringWithFormat:@"bag%d.png", j+1];
                                break;
                            case 3:
                                image = [NSString stringWithFormat:@"belt%d.png", j+1];
                                break;
                            case 4:
                                image = [NSString stringWithFormat:@"jew%d.png", j+1];
                                break;
                            case 5:
                                image = [NSString stringWithFormat:@"hair%d.png", j+1];
                                break;
                            case 6:
                                image = [NSString stringWithFormat:@"glasses%d.png", j+1];
                                break;
                            case 7:
                                image = [NSString stringWithFormat:@"hat%d.png", j+1];
                                break;
                            case 8:
                                image = [NSString stringWithFormat:@"jacket%d.png", j+1];
                                break;
                            case 9:
                                image = [NSString stringWithFormat:@"top%d.png", j+1];
                                break;
                            case 10:
                                image = [NSString stringWithFormat:@"pants%d.png", j+1];
                                break;
                            case 11:
                                image = [NSString stringWithFormat:@"skirt%d.png", j+1];
                                break;
                            case 12:
                                image = [NSString stringWithFormat:@"dress%d.png", j+1];
                                break;
                            case 13:
                                image = [NSString stringWithFormat:@"stockings%d.png", j+1];
                                break;
                            default:
                                break;
                        }
                        CCSprite *mySprite = [CCSprite spriteWithSpriteFrameName:image];
                        mySprite.position = gamepoint[i];
                        [self addChild:mySprite z:[[level_item objectAtIndex:i] integerValue] tag:501+i];
                        //NSLog(@"501+i = %d", (501+i));
                        if ((i==717-501)||(i==718-501)) {[self getChildByTag:8].visible=YES;}
                        
                        mySprite1.opacity=155;
                    }
                    ++j;
                }
            } else {
                if (mySprite1.tag>[[first_item objectAtIndex:active_category-1] integerValue]) {
                    if (mySprite1.tag==100+active_elements) {
                        image = [NSString stringWithFormat:@"shoes%d.png", j+1];
                        CCSprite *mySprite = [CCSprite spriteWithSpriteFrameName:image];
                        mySprite.position = gamepoint[i];
                        [self addChild:mySprite z:[[level_item objectAtIndex:i] integerValue] tag:501+i];
                        
                        mySprite1.opacity=155;
                    }
                    ++j;
                }
            }
            ++i;
        }
    }
}

- (void) panelelementsmoved :(CGPoint)translation {
    for (CCSprite *mySprite1 in movableelements) {
        if (active_category<14) {
            if ((mySprite1.tag>[[first_item objectAtIndex:active_category-1] integerValue])&&(mySprite1.tag<[[first_item objectAtIndex:active_category] integerValue]+1)) {
                mySprite1.position = CGPointMake(mySprite1.position.x+translation.x, mySprite1.position.y);
            }
        } else {
            if (mySprite1.tag>[[first_item objectAtIndex:active_category-1] integerValue]) {
                mySprite1.position = CGPointMake(mySprite1.position.x+translation.x, mySprite1.position.y);
            }
        }
    }
}

-(void) movepanelelementstoEnd:(CGPoint)velocity_user {
    Boolean found=NO;
    int first = 1;
    int vsego=0;
    if (velocity_user.x>0) {
        velocity_user.x=150;
    } else {
        velocity_user.x=-150;
    }
    for (CCSprite *mySprite1 in movableelements) {
        [mySprite1 stopAllActions];
        if (active_category<14) {
            if ((mySprite1.tag>[[first_item objectAtIndex:active_category-1] integerValue])&&(mySprite1.tag<[[first_item objectAtIndex:active_category] integerValue]+1)) {
                vsego++;
                if (found==NO) {
                    if (mySprite1.position.x+velocity_user.x>0) {
                        first=vsego;
                        found=YES;
                    }
                }
            }
        } else {
            if (mySprite1.tag>[[first_item objectAtIndex:active_category-1] integerValue]) {
                vsego++;
                if (found==NO) {
                    if (mySprite1.position.x+velocity_user.x>0) {
                        first=vsego;
                        found=YES;
                    }
                }
            }
        }
    }
    
    int new_first=0;
    if (found==NO) {first=vsego;}
    new_first=first-(first%4)+1;
    if (first==vsego) {if (first%4==0) {new_first=first-3;}}
    
    int count_pages;
    if (active_category!=14) {
        if (([[first_item objectAtIndex:active_category] integerValue]-[[first_item objectAtIndex:active_category-1] integerValue])%4==0) {
            count_pages=([[first_item objectAtIndex:active_category] integerValue]-[[first_item objectAtIndex:active_category-1] integerValue])/4;
        } else {
            count_pages=([[first_item objectAtIndex:active_category] integerValue]-[[first_item objectAtIndex:active_category-1] integerValue]-([[first_item objectAtIndex:active_category] integerValue]-[[first_item objectAtIndex:active_category-1] integerValue])%4)/4+1;
        }
    } else {
        count_pages=5;
    }
    
    int active_page;
    if (new_first%4==0) {
        active_page=new_first/4;
    } else {
        active_page=(new_first-new_first%4)/4+1;
    }
    for(int i = 0; i < 8; ++i) {
        if (i>count_pages-1) {
            [self getChildByTag:9001+i].visible=NO;
        } else {
            CCSprite *temp = (CCSprite *) [self getChildByTag:9001+i];
            if (active_page==i+1) {
                temp.color=ccWHITE;
            } else {
                temp.color=ccBLACK;
            }
        }
    }
    
    CGPoint Velocity = CGPointZero;
    Velocity.y=0;
    vsego=0;
    for (CCSprite *mySprite1 in movableelements) {
        if (active_category<14) {
            if ((mySprite1.tag>[[first_item objectAtIndex:active_category-1] integerValue])&&(mySprite1.tag<[[first_item objectAtIndex:active_category] integerValue]+1)) {
                vsego++;
                if (vsego==new_first) {
                    Velocity.x=x_between/2+x_maxmenu/2-mySprite1.position.x;
                    break;
                }
            }
        } else {
            if (mySprite1.tag>[[first_item objectAtIndex:active_category-1] integerValue]) {
                vsego++;
                if (vsego==new_first) {
                    Velocity.x=x_between/2+x_maxmenu/2-mySprite1.position.x;
                    break;
                }
            }
        }
        
    }
    for (CCSprite *mySprite1 in movableelements) {
        if (active_category<14) {
            if ((mySprite1.tag>[[first_item objectAtIndex:active_category-1] integerValue])&&(mySprite1.tag<[[first_item objectAtIndex:active_category] integerValue]+1)) {
                [mySprite1 runAction:[CCEaseExponentialOut actionWithAction:[CCMoveTo actionWithDuration:0.5 position:ccp(mySprite1.position.x+Velocity.x, mySprite1.position.y)]]];
            }
        } else {
            if (mySprite1.tag>[[first_item objectAtIndex:active_category-1] integerValue]) {
                [mySprite1 runAction:[CCEaseExponentialOut actionWithAction:[CCMoveTo actionWithDuration:0.5 position:ccp(mySprite1.position.x+Velocity.x, mySprite1.position.y)]]];
            }
        }
        
    }
}

-(void) unlock_trial {
    //разблокировка на лету
    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
    NSNumber *value1 = [NSNumber numberWithInt:777];
    [ud setObject:value1 forKey:@"WalksinParis_v"];
    [ud synchronize];
    [Singleton sharedSingleron].Trial_Mode=NO;
    int i=0;
    for (CCSprite *mySprite1 in movableelements) {
        [mySprite1 removeChildByTag:10101+i cleanup:YES];
        ++i;
    }
    if (reklama_visible==YES) {
        // Reset delegate
        [FlurryAds removeAdFromSpace:@"WiP_iPhone_bantop"];
        mainmenu.position = ccp(mainmenu.position.x, mainmenu.position.y+40);
    }
    [Singleton sharedSingleron].Show_Reklama=NO;
    [self unscheduleAllSelectors];
}

#pragma mark -
#pragma mark === UIActionViewDelegate Methods ===
#pragma mark -

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==77777) {
        NSLog(@"facebook & Email");
        switch (buttonIndex) {
            case 1:
                [self sendfacebook];
                dialog1=NO;
                break;
            case 2:
                [self sendmail];
                dialog1=NO;
                break;
            default:
                [[SimpleAudioEngine sharedEngine] playEffect:@"button4-1.caf"];
                dialog1=NO;
                break;
        }
    }
    if (alertView.tag==77778) {
        NSLog(@"Bye");
        switch (buttonIndex) {
            case 1:
                [self okeypressedtrial];
                dialog1=NO;
                break;
            case 2:
                [self restorepressedtrial];
                dialog1=NO;
                break;
            default:
                [[SimpleAudioEngine sharedEngine] playEffect:@"button4-1.caf"];
                dialog1=NO;
                break;
        }
    }
}

// вызов окна предложения покупки полной версии

-(void) window_buy{
    if (dialog1==NO) {
        [[SimpleAudioEngine sharedEngine] playEffect:@"button4-1.caf"];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                            message:NSLocalizedString(@"Unlock all items?", nil)
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                                  otherButtonTitles:NSLocalizedString(@"Buy", nil), NSLocalizedString(@"Restore purchase", nil), nil];
        [alertView show];
        [alertView setTag:77778];
        dialog1=YES;
    }
}

-(void) okeypressedtrial {
    [[SimpleAudioEngine sharedEngine] playEffect:@"button4-1.caf"];
    //dialog1=NO;
    //rezultdialog.position = ccp(-winsize.width/2, -winsize.height/2);
    //вызов процедуры разблокировки - должна быть после проверки оплаты
    //[self unlock_trial];
    //InAPP
    Reachability *reach = [Reachability reachabilityForInternetConnection];
    NetworkStatus netStatus = [reach currentReachabilityStatus];
    if (netStatus == NotReachable) {
        //NSLog(@"No internet connection!");
    } else {
        if ([InAppRageIAPHelper sharedHelper].products == nil) {
            [[InAppRageIAPHelper sharedHelper] requestProducts];
        } else {
            [[NSNotificationCenter defaultCenter] postNotificationName:kProductsLoadedNotification object:[InAppRageIAPHelper sharedHelper].products];
        }
    }
}

- (void)productPurchased:(NSNotification *)notification {
    [Flurry logEvent:@"Complete_Buy"];
    //InApp
    [self unlock_trial];
}

- (void)productPurchaseFailed:(NSNotification *)notification {
    [Flurry logEvent:@"Purchase_Failed"];
    //InApp
    SKPaymentTransaction * transaction = (SKPaymentTransaction *) notification.object;
    if (transaction.error.code != SKErrorPaymentCancelled) {
        UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error!", nil)
                                                         message:transaction.error.localizedDescription
                                                        delegate:nil
                                               cancelButtonTitle:nil
                                               otherButtonTitles:@"OK", nil] autorelease];
        
        [alert show];
    }
}

- (void)productsLoaded:(NSNotification *)notification {
    //InApp
    SKProduct *product = [[InAppRageIAPHelper sharedHelper].products objectAtIndex:0];
    [[InAppRageIAPHelper sharedHelper] buyProductIdentifier:product.productIdentifier];
}

//ресторе покупки
-(void) restorepressedtrial {
    [Flurry logEvent:@"Restore_Buy"];
    [[SimpleAudioEngine sharedEngine] playEffect:@"0_buttons.caf"];
    //dialog1=NO;
    //rezultdialog.position = ccp(-winsize.width/2, -winsize.height/2);
    [[InAppRageIAPHelper sharedHelper] restoreCompletedTransactions];
}

- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
    if (dialog1==NO) {
        CGPoint touchLocation = [touch locationInView: [touch view]];
        touchLocation = [[CCDirector sharedDirector] convertToGL: touchLocation];
        touchLocation = [self convertToNodeSpace:touchLocation];
        //NSLog(@"%f,%f", touchLocation.x, touchLocation.y);
        startTouchPosition = touchLocation;
        velocityTouch = ccp(0,0);
        beforeTouchPosition = touchLocation;
        
        
        /*if (maindialog.position.x==winsize.width/2) {
            CGRect mySurface = CGRectMake(14, 176, 306-14, 319-176);
            if (!CGRectContainsPoint(mySurface, touchLocation)) {
                maindialog.position = ccp(-winsize.width/2, -winsize.height/2);
            }
            
        } else {*/
            if (CGRectContainsPoint([self getChildByTag:10].boundingBox, touchLocation)) {
                for (int i = 0; i < 27; ++i) {
                    CCSprite *sprite_for_reorder = (CCSprite *) [self getChildByTag:640+i];
                    if (sprite_for_reorder!=nil) {
                        if (sprite_for_reorder.zOrder==60) {[self reorderChild:sprite_for_reorder z:18];} else {[self reorderChild:sprite_for_reorder z:60];}
                    }
                }
            }
        //}
    }
    return TRUE;
}

- (void)ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event {
    CGPoint touchLocation = [touch locationInView: [touch view]];
    touchLocation = [[CCDirector sharedDirector] convertToGL: touchLocation];
    touchLocation = [self convertToNodeSpace:touchLocation];
    
    velocityTouch = ccp(-beforeTouchPosition.x+touchLocation.x,-beforeTouchPosition.y+touchLocation.y);
    beforeTouchPosition = touchLocation;
    
    //if (CGRectContainsPoint([self getChildByTag:5].boundingBox, startTouchPosition)) {
    //    [self panelofelementsmoved:velocityTouch];
    //}
    CGRect mySurface = CGRectMake(winsize.width-between_button_x*2-size_button, [self getChildByTag:5].contentSize.height, between_button_x*2+size_button, winsize.height-[self getChildByTag:5].contentSize.height);
    //NSLog(@"startTouchPosition x=%f, y=%f",startTouchPosition.x, startTouchPosition.y);
    if (CGRectContainsPoint(mySurface, startTouchPosition)) {
        [self panelofbuttonmoved:velocityTouch];
    }
    if (CGRectContainsPoint([self getChildByTag:5].boundingBox, startTouchPosition)) {
        [self panelelementsmoved:velocityTouch];
    }
}

-(void) ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event {

    CGPoint touchLocation = [touch locationInView: [touch view]];
    touchLocation = [[CCDirector sharedDirector] convertToGL: touchLocation];
    touchLocation = [self convertToNodeSpace:touchLocation];
    
    //NSLog(@"velocity x=%f, y=%f", velocityTouch.x, velocityTouch.y);
    
    if (sqrtf((startTouchPosition.x-touchLocation.x)*(startTouchPosition.x-touchLocation.x)+(startTouchPosition.y-touchLocation.y)*(startTouchPosition.y-touchLocation.y))>15) {
        CGRect mySurface = CGRectMake(winsize.width-between_button_x*2-size_button, [self getChildByTag:5].contentSize.height, between_button_x*2+size_button, winsize.height-[self getChildByTag:5].contentSize.height);
        if (CGRectContainsPoint(mySurface, startTouchPosition)) {
            [self reversepanelbuttonwithvelocity:velocityTouch];
        }
        if (CGRectContainsPoint([self getChildByTag:5].boundingBox, startTouchPosition)) {
            [self movepanelelementstoEnd:velocityTouch];
        }
    } else {
        /*if (maindialog.position.x==winsize.width/2) {
            CGRect mySurface = CGRectMake(14, 176, 306-14, 319-176);
            if (!CGRectContainsPoint(mySurface, touchLocation)) {
                maindialog.position = ccp(-winsize.width/2, -winsize.height/2);
            }
            
        } else {*/
            //выбрана новая категория
            CGRect mySurface = CGRectMake(winsize.width-between_button_x*2-size_button, [self getChildByTag:5].contentSize.height, between_button_x*2+size_button, winsize.height-[self getChildByTag:5].contentSize.height);
            if (CGRectContainsPoint(mySurface, touchLocation)) {
                int i=0;
                for (CCSprite *mySprite1 in movablebutton) {
                    if (CGRectContainsPoint(mySprite1.boundingBox, touchLocation)) {
                        active_category=i+1;
                        [self select_category];
                        [[SimpleAudioEngine sharedEngine] playEffect:@"button4-1.caf"];
                    }
                    i++;
                }
            }
            
            //выбран элемент
            if (CGRectContainsPoint([self getChildByTag:5].boundingBox, touchLocation)) {
                active_elements=0;
                int i=0;
                BOOL found=NO;
                for (CCSprite *mySprite1 in movableelements) {
                    if (((active_category<14)&&(mySprite1.tag>[[first_item objectAtIndex:active_category-1] integerValue])&&(mySprite1.tag<[[first_item objectAtIndex:active_category] integerValue]+1))||((active_category==14)&&(mySprite1.tag>[[first_item objectAtIndex:active_category-1] integerValue]))) {
                        if (CGRectContainsPoint(mySprite1.boundingBox, touchLocation)) {
                            if (([Singleton sharedSingleron].Trial_Mode==YES)&&([mySprite1 getChildByTag:10101+i].visible==YES)) {
                                [self window_buy];
                            } else {
                                active_elements=i+1;
                                [self select_elements];
                            }
                            found=YES;
                            break;
                        }
                    }
                    i++;
                }
                CGRect mySurface = CGRectMake(0, 0, 0, 0);
                for (int j=1; j<5; j++) {
                    if ((active_elements==0)&&(found==NO)) {
                        mySurface = CGRectMake(touchLocation.x-5*j, touchLocation.y-5*j, 10*j, 10*j);
                        i=0;
                        for (CCSprite *mySprite1 in movableelements) {
                            if (((active_category<14)&&(mySprite1.tag>[[first_item objectAtIndex:active_category-1] integerValue])&&(mySprite1.tag<[[first_item objectAtIndex:active_category] integerValue]+1))||((active_category==14)&&(mySprite1.tag>[[first_item objectAtIndex:active_category-1] integerValue]))) {
                                if (CGRectIntersectsRect(mySprite1.boundingBox, mySurface)) {
                                    if (([Singleton sharedSingleron].Trial_Mode==YES)&&([mySprite1 getChildByTag:10101+i].visible==YES)) {
                                        [self window_buy];
                                    } else {
                                        active_elements=i+1;
                                        [self select_elements];
                                    }
                                    found=YES;
                                    break;
                                }
                            }
                            i++;
                        }
                    }
                }
            //}
        }
        if (sqrtf((startTouchPosition.x-touchLocation.x)*(startTouchPosition.x-touchLocation.x)+(startTouchPosition.y-touchLocation.y)*(startTouchPosition.y-touchLocation.y))>5) {
            CGRect mySurface = CGRectMake(winsize.width-between_button_x*2-size_button, [self getChildByTag:5].contentSize.height, between_button_x*2+size_button, winsize.height-[self getChildByTag:5].contentSize.height);
            if (CGRectContainsPoint(mySurface, startTouchPosition)) {
                [self reversepanelbuttonwithvelocity:velocityTouch];
            }
            if (CGRectContainsPoint([self getChildByTag:5].boundingBox, startTouchPosition)) {
                [self movepanelelementstoEnd:velocityTouch];
            }
        }
    }
}

-(void) ccTouchCancelled:(UITouch *)touch withEvent:(UIEvent *)event
{
	//[self ccTouchEnded:touch withEvent:event];
}

- (void)onEnter {
    // Добавляем делагат в CCTouchDispatcher для получения тачей
    //[[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:NO];
    CCDirectorIOS* director_ = (CCDirectorIOS*) [CCDirector sharedDirector];
    [[director_ touchDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:NO];
    [super onEnter];
    if ([Singleton sharedSingleron].Show_Reklama==YES) {
        // Fetch and display banner ad
        /*if ([FlurryAds adReadyForSpace:@"WiP_iPhone_bantop"]) {
            [FlurryAds displayAdForSpace:@"WiP_iPhone_bantop" onView:dir.view];
        } else {
            [FlurryAds fetchAdForSpace:@"WiP_iPhone_bantop" frame:dir.view.frame size:BANNER_TOP];
        }*/
        [FlurryAds fetchAndDisplayAdForSpace:@"WiP_iPhone_bantop" view:[CCDirector sharedDirector].view size:BANNER_TOP];
        [self scheduleUpdate];
        reklama_visible=YES;
        mainmenu.position = ccp(mainmenu.position.x, mainmenu.position.y-40);
    }
}

// апдейт шедулера
-(void) update:(ccTime)delta
{
    time_for_flurry=time_for_flurry+delta;
    if (reklama_visible==YES) {
        if (time_for_flurry>time1) {
            [FlurryAds removeAdFromSpace:@"WiP_iPhone_bantop"];
            time_for_flurry=0;
            reklama_visible=NO;
            mainmenu.position = ccp(mainmenu.position.x, mainmenu.position.y+40);
        }
    } else {
        if ((time_for_flurry>time2)&&(time2!=0)) {
            [FlurryAds fetchAndDisplayAdForSpace:@"WiP_iPhone_bantop" view:[CCDirector sharedDirector].view size:BANNER_TOP];
            time_for_flurry=0;
            reklama_visible=YES;
            mainmenu.position = ccp(mainmenu.position.x, mainmenu.position.y-40);
        }
    }
}

-(void) cleanup
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kProductsLoadedNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kProductPurchasedNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kProductPurchaseFailedNotification object: nil];
    
    // Удаляем делегат
    //[[CCTouchDispatcher sharedDispatcher] removeDelegate:self];
	CCDirectorIOS* director_ = (CCDirectorIOS*) [CCDirector sharedDirector];
    [[director_ touchDispatcher] removeDelegate:self];
	if ([Singleton sharedSingleron].Show_Reklama==YES) {
        // Reset delegate
        [FlurryAds removeAdFromSpace:@"WiP_iPhone_bantop"];
    }
    [self unscheduleAllSelectors];
    
    [super cleanup];
}

- (void)dealloc {
    [movablebutton release];
    movablebutton = nil;
    
    [movableelements release];
    movableelements = nil;
    
    [first_item release];
    level_item = nil;    
    
    [level_item release];
    level_item = nil;
    
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    [[CCSpriteFrameCache sharedSpriteFrameCache] removeUnusedSpriteFrames];
    
    [[CCTextureCache sharedTextureCache] removeAllTextures];
    [[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFrames];
    [self removeAllChildrenWithCleanup:YES];
    
    [super dealloc];
}

@end
