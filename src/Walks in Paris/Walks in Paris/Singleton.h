//
//  Singleton.h
//  Walks in Paris
//
//  Created by Andrey Zavaliy on 14.01.13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Singleton : NSObject{
    BOOL _Trial_Mode;
    int _number_model;
    BOOL _Show_Reklama;
    BOOL _rerun_choise;
}
+ (Singleton *) sharedSingleron;

@property (readwrite) BOOL Trial_Mode;
@property (readwrite) int number_model;
@property (readwrite) BOOL Show_Reklama;
@property (readwrite) BOOL rerun_choise;

@end