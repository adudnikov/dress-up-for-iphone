//
//  HelloWorldLayer.h
//  Walks in Paris
//
//  Created by Andrey Zavaliy on 28.12.12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//


#import <GameKit/GameKit.h>

// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"
#import "FBConnect.h"

// HelloWorldLayer
@interface HelloWorldLayer : CCLayer //<GKAchievementViewControllerDelegate, GKLeaderboardViewControllerDelegate>
{
    BOOL can_back_change;
    CGPoint startTouchPosition,velocityTouch,beforeTouchPosition;
    int size_button,between_button_x,between_button_y;
    CGSize winsize;
    
    NSMutableArray *movablebutton, *movableelements;
    int x_maxmenu,x_between;
    int active_category, active_elements;
    
    NSMutableArray *first_item, *level_item;
    
    //CCSprite *maindialog, *rezultdialog;
    //CCMenu *maindialogmenu; //*button_ok;
    //CCLabelTTF *titledialog, /**titlerezultdialog, */*messagerezult;
    //CCMenu *button_cancel_trial, *button_ok_trial, *button_restore_trial;
    //CCMenu *mainmenu, *menu;
    Boolean dialog1;
    UIAlertView *alert_wait;
    Facebook *facebook;
    BOOL isFBLogged;
    CCMenuItem *facebookLoginButton, *facebookLogoutButton;
    
    CCMenu *mainmenu;
    BOOL button_can_touch;
    float time_for_flurry;
    BOOL reklama_visible;
}

// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;

@end
