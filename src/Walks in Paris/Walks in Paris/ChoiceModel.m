//
//  ChoiceModel.m
//  Walks in Paris
//
//  Created by Andrey Zavaliy on 21.01.13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

// Import the interfaces
#import "ChoiceModel.h"
#import "HelloWorldLayer.h"
#import "SimpleAudioEngine.h"
#import "Singleton.h"
#import "Flurry.h"
#import "FlurryAds.h"
#import "FlurryAdDelegate.h"

#pragma mark - ChoiceModel

// HelloWorldLayer implementation
@implementation ChoiceModel

// Helper class method that creates a Scene with the HelloWorldLayer as the only child.
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	ChoiceModel *layer = [ChoiceModel node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super's" return value
	if( (self=[super init]) ) {
		winsize = [[CCDirector sharedDirector] winSize];
		
		[[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"choice_model.plist"];
        [[CCTextureCache sharedTextureCache] addImage:@"choice_model.pvr.ccz" ];
        
        CCSprite *background = [CCSprite spriteWithSpriteFrameName:@"choice_panorama.png"];
        background.position = ccp(winsize.width, winsize.height/2);
        [self addChild:background z:1 tag:1];
        
        CCSprite *model2 = [CCSprite spriteWithSpriteFrameName:@"model2_dressed.png"];
        model2.position = ccp(background.contentSize.width/2, winsize.height/2-30);
        [background addChild:model2 z:1 tag:12];
        CCSprite *hand2 = [CCSprite spriteWithSpriteFrameName:@"model2_ruka.png"];
        hand2.position = ccp(model2.contentSize.width/2,model2.contentSize.height/2);
        [model2 addChild:hand2 z:2];
        
        CCSprite *model1 = [CCSprite spriteWithSpriteFrameName:@"model1_dressed.png"];
        model1.position = ccp(background.contentSize.width/2-model2.contentSize.width*4/3, winsize.height/2-30);
        [background addChild:model1 z:1 tag:11];
        CCSprite *hand1 = [CCSprite spriteWithSpriteFrameName:@"model1_ruka.png"];
        hand1.position = ccp(model1.contentSize.width/2,model1.contentSize.height/2);
        [model1 addChild:hand1 z:2];
        
        CCSprite *model3 = [CCSprite spriteWithSpriteFrameName:@"model3_dressed.png"];
        model3.position = ccp(background.contentSize.width/2+model2.contentSize.width*4/3, winsize.height/2-30);
        [background addChild:model3 z:1 tag:13];
        CCSprite *hand3 = [CCSprite spriteWithSpriteFrameName:@"model3_ruka.png"];
        hand3.position = ccp(model3.contentSize.width/2,model3.contentSize.height/2);
        [model3 addChild:hand3 z:2];
        
        [[CCTextureCache sharedTextureCache] removeUnusedTextures];
        [[CCSpriteFrameCache sharedSpriteFrameCache] removeUnusedSpriteFrames];
    }
    
	return self;
}

-(void) go_nextscene {
    [[SimpleAudioEngine sharedEngine] playEffect:@"button4-1.caf"];
    [[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:1 scene:[HelloWorldLayer scene]]];
}

-(void) reversebackgroundwithvelocity:(CGPoint)velocity_user {
    velocity_user.x=velocity_user.x*4;
    
    if ([self getChildByTag:1].position.x+velocity_user.x>winsize.width) {
        [[self getChildByTag:1] runAction:[CCEaseExponentialOut actionWithAction:[CCMoveTo actionWithDuration:1 position:ccp(winsize.width, [self getChildByTag:1].position.y)]]];
    } else {
        if ([self getChildByTag:1].position.x+velocity_user.x<0) {
            [[self getChildByTag:1] runAction:[CCEaseExponentialOut actionWithAction:[CCMoveTo actionWithDuration:1 position:ccp(0, [self getChildByTag:1].position.y)]]];
        } else {
            [[self getChildByTag:1] runAction:[CCEaseExponentialOut actionWithAction:[CCMoveTo actionWithDuration:1 position:ccp([self getChildByTag:1].position.x+velocity_user.x, [self getChildByTag:1].position.y)]]];
        }
    }
    
}

- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
        CGPoint touchLocation = [touch locationInView: [touch view]];
        touchLocation = [[CCDirector sharedDirector] convertToGL: touchLocation];
        touchLocation = [self convertToNodeSpace:touchLocation];
        
        startTouchPosition = touchLocation;
        velocityTouch = ccp(0,0);
        beforeTouchPosition = touchLocation;
    
    return TRUE;
}

- (void)ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event {
    CGPoint touchLocation = [touch locationInView: [touch view]];
    touchLocation = [[CCDirector sharedDirector] convertToGL: touchLocation];
    touchLocation = [self convertToNodeSpace:touchLocation];
    
    velocityTouch = ccp(-beforeTouchPosition.x+touchLocation.x,-beforeTouchPosition.y+touchLocation.y);
    beforeTouchPosition = touchLocation;
    [[self getChildByTag:1] stopAllActions];
    if (([self getChildByTag:1].position.x+velocityTouch.x<winsize.width*1.5)&&([self getChildByTag:1].position.x+velocityTouch.x>-winsize.width*0.5)) {
        [self getChildByTag:1].position= CGPointMake([self getChildByTag:1].position.x+velocityTouch.x, [self getChildByTag:1].position.y);
    }
}

-(void) ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event {
    CGPoint touchLocation = [touch locationInView: [touch view]];
    touchLocation = [[CCDirector sharedDirector] convertToGL: touchLocation];
    touchLocation = [self convertToNodeSpace:touchLocation];

    CGPoint touchLocation_local = [[CCDirector sharedDirector] convertToGL:touchLocation];
    touchLocation_local = [[self getChildByTag:1] convertToNodeSpace:touchLocation];

    if (sqrtf((startTouchPosition.x-touchLocation.x)*(startTouchPosition.x-touchLocation.x)+(startTouchPosition.y-touchLocation.y)*(startTouchPosition.y-touchLocation.y))>15) {
        [self reversebackgroundwithvelocity:velocityTouch];
    } else {
        if (CGRectContainsPoint([[self getChildByTag:1] getChildByTag:11].boundingBox, touchLocation_local)) {
            [Singleton sharedSingleron].number_model=1;
            [self go_nextscene];
        } else {
            if (CGRectContainsPoint([[self getChildByTag:1] getChildByTag:12].boundingBox, touchLocation_local)) {
                [Singleton sharedSingleron].number_model=2;
                [self go_nextscene];
            } else {
                if (CGRectContainsPoint([[self getChildByTag:1] getChildByTag:13].boundingBox, touchLocation_local)) {
                    [Singleton sharedSingleron].number_model=3;
                    [self go_nextscene];
                }
            }
        }
        if (sqrtf((startTouchPosition.x-touchLocation.x)*(startTouchPosition.x-touchLocation.x)+(startTouchPosition.y-touchLocation.y)*(startTouchPosition.y-touchLocation.y))>5) {
            [self reversebackgroundwithvelocity:velocityTouch];
        }
    }
}

-(void) ccTouchCancelled:(UITouch *)touch withEvent:(UIEvent *)event
{
	[self ccTouchEnded:touch withEvent:event];
}

- (void)onEnter {
    // Добавляем делагат в CCTouchDispatcher для получения тачей
    CCDirectorIOS* director_ = (CCDirectorIOS*) [CCDirector sharedDirector];
    [[director_ touchDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:NO];
    
    if (([Singleton sharedSingleron].Show_Reklama==YES)&&([Singleton sharedSingleron].rerun_choise==YES)) {
        CCDirector* dir = [CCDirector sharedDirector];
        // Fetch and display banner ad
        if ([FlurryAds adReadyForSpace:@"WiP_iPhone_fullscr"]) {
            [FlurryAds displayAdForSpace:@"WiP_iPhone_fullscr" onView:dir.view];
        } else {
            [FlurryAds fetchAdForSpace:@"WiP_iPhone_fullscr" frame:dir.view.frame size:FULLSCREEN];
        }
        //[FlurryAds fetchAndDisplayAdForSpace:@"WiP_iPhone_fullscr" view:[CCDirector sharedDirector].view size:FULLSCREEN];
    }
    [super onEnter];
}

-(void) cleanup
{    
    // Удаляем делегат
	CCDirectorIOS* director_ = (CCDirectorIOS*) [CCDirector sharedDirector];
    [[director_ touchDispatcher] removeDelegate:self];
	
    [super cleanup];
}

- (void)dealloc {
    if ([Singleton sharedSingleron].Show_Reklama==YES) {
        // Reset delegate
        [FlurryAds removeAdFromSpace:@"WiP_iPhone_fullscr"];
    }
    [self removeAllChildrenWithCleanup:YES];
    [super dealloc];
}

@end