//
//  ChoiceModel.h
//  Walks in Paris
//
//  Created by Andrey Zavaliy on 21.01.13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"

// HelloWorldLayer
@interface ChoiceModel : CCLayer
{
    CGSize winsize;
    CGPoint startTouchPosition,velocityTouch,beforeTouchPosition;
}

// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;

@end