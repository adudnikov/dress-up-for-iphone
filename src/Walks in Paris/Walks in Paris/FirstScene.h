//
//  FirstScene.h
//  Walks in Paris
//
//  Created by Andrey Zavaliy on 22.01.13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"

// HelloWorldLayer
@interface FirstScene : CCLayer
{
}

// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;

@end
