//
//  MailSender.m
//  Walks in Paris
//
//  Created by Andrey Zavaliy on 14.01.13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "MailSender.h"
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "cocos2d.h"
#import "CCScreenshot.h"

@implementation MailSender

- (void)openMail:(UIImage*)image
{
    
    CCGLView *view = (CCGLView *)[[CCDirector sharedDirector] view];
    
    // If these three lines are commented, the mail dialogue window will be shown incorrectly (rotated 90º)
    //CGAffineTransform rotate = CGAffineTransformMakeRotation(3.14/2);
    //rotate = CGAffineTransformScale(rotate, 0.1, 0.8);
    //[view setTransform:rotate];
    
    [view addSubview:self.view];

    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
                
        mailer.mailComposeDelegate = self;
        
        [mailer setSubject:NSLocalizedString(@"My Look - Walks in Paris Dress Up", nil)];
        
        NSString *emailBody = NSLocalizedString(@"I created this look on my iPhone with Walks in Paris Dress Up", nil);
        [mailer setMessageBody:emailBody isHTML:NO];
        
        NSData *imageData = UIImageJPEGRepresentation(image, 1);
        [mailer addAttachmentData:imageData mimeType:@"image/png" fileName:@"mylook.jpg"];
        // only for iPad
        // mailer.modalPresentationStyle = UIModalPresentationPageSheet;
        
        [self presentModalViewController:mailer animated:YES];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                        message:@"Your device doesn't support the composer sheet"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];
        [alert release];
    }
    
}

#pragma mark - MFMailComposeController delegate

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
	switch (result)
	{
            UIAlertView *alert;
        case MFMailComposeResultCancelled:
			NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued");
            alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Oops", nil)
                                               message:NSLocalizedString(@"Mail cancelled: you cancelled the operation and no email message was queued", nil)
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles: nil];
            [alert show];
            [alert release];
			break;
		case MFMailComposeResultSaved:
			NSLog(@"Mail saved: you saved the email message in the Drafts folder");
            alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Success!", nil)
                                               message:NSLocalizedString(@"Mail saved: you saved the email message in the Drafts folder", nil)
                                              delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles: nil];
            [alert show];
            [alert release];
			break;
		case MFMailComposeResultSent:
			NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send the next time the user connects to email");
            alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Success!", nil)
                                               message:NSLocalizedString(@"Mail send: the email message is queued in the outbox. It is ready to send the next time the user connects to email", nil)
                                              delegate:nil
                                     cancelButtonTitle:@"OK"
                                     otherButtonTitles: nil];
            [alert show];
            [alert release];
			break;
		case MFMailComposeResultFailed:
			NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error");
            alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error!", nil)
                                               message:NSLocalizedString(@"Mail failed: the email message was not saved or queued, possibly due to an error", nil)
                                              delegate:nil
                                     cancelButtonTitle:@"OK"
                                     otherButtonTitles: nil];
            [alert show];
            [alert release];
			break;
		default:
			//NSLog(@"Mail not sent");
            alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error!", nil)
                                               message:NSLocalizedString(@"Mail not sent", nil)
                                              delegate:nil
                                     cancelButtonTitle:@"OK"
                                     otherButtonTitles: nil];
            [alert show];
            [alert release];
			break;
	}
	[self dismissModalViewControllerAnimated:YES];
}

@end
