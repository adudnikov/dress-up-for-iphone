//
//  CCScreenshot.m
//  Walks in Paris
//
//  Created by Andrey Zavaliy on 14.01.13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "CCScreenshot.h"

@implementation CCScreenshot

+(UIImage*) screenshotWithStartNode:(CCNode*)startNode
{
	[CCDirector sharedDirector].nextDeltaTimeZero = YES;
	
	CGSize winSize = [CCDirector sharedDirector].winSize;
    
    CCRenderTexture* rtx = [CCRenderTexture renderTextureWithWidth:winSize.width height:winSize.height];

	[rtx begin];

	[startNode visit];
	
	[rtx end];
    
    int delta=90;
    
    CCSprite *textureSprite = [CCSprite spriteWithTexture:rtx.sprite.texture];
    textureSprite.position = ccp(winSize.width/2, winSize.height/2-delta);
    textureSprite.scaleY=-1;
    rtx = [CCRenderTexture renderTextureWithWidth:winSize.width height:winSize.height-delta];
    
    [rtx begin];
    [textureSprite visit];
    [rtx end];
    
	return [rtx getUIImage];
}

@end
