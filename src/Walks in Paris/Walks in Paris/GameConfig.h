//
//  GameConfig.h
//  Walks in Paris
//
//  Created by Andrey Zavaliy on 28.02.13.
//
//

#ifndef Walks_in_Paris_GameConfig_h
#define Walks_in_Paris_GameConfig_h

#define time1 15 //время показа рекламного баннера (секунд)
#define time2 15 //время непоказа рекламного баннера (секунд) если 0 то не будет появляться после первого исчезновения
#define days_until_reklama 0 //дней до показа баннера рекламы
#define session_until_reklama 2 //сессий до показа баннера рекламы (если установить 2 то на 3ей сессии уже может будет реклама)

#endif
