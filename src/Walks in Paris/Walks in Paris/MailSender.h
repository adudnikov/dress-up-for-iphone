//
//  MailSender.h
//  Walks in Paris
//
//  Created by Andrey Zavaliy on 14.01.13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface MailSender : UIViewController <MFMailComposeViewControllerDelegate>

- (void)openMail:(UIImage*)image;

@end
