//
//  CCScreenshot.h
//  Walks in Paris
//
//  Created by Andrey Zavaliy on 14.01.13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface CCScreenshot : NSObject

+(UIImage*) screenshotWithStartNode:(CCNode*)startNode;
@end
