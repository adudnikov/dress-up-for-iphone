//
//  FirstScene.m
//  Walks in Paris
//
//  Created by Andrey Zavaliy on 22.01.13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

// Import the interfaces
#import "FirstScene.h"
#import "ChoiceModel.h"
#import "SimpleAudioEngine.h"

#pragma mark - FirstScene

// HelloWorldLayer implementation
@implementation FirstScene

// Helper class method that creates a Scene with the HelloWorldLayer as the only child.
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	FirstScene *layer = [FirstScene node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super's" return value
	if( (self=[super init]) ) {
		CGSize winsize = [[CCDirector sharedDirector] winSize];
		
        [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"paris_sound-1.caf" loop:YES ];
        [SimpleAudioEngine sharedEngine].backgroundMusicVolume=0.4f;
		
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"launch.plist"];
        [[CCTextureCache sharedTextureCache] addImage:@"launch.pvr.ccz" ];
        
        CCSprite *background1 = [CCSprite spriteWithSpriteFrameName:@"launch_back.png"];
        background1.position = ccp(winsize.width/2, winsize.height/2);
        [self addChild:background1 z:1];
        
        CCSprite *temp1 = [CCSprite spriteWithSpriteFrameName:@"start_released.png"];
        CCSprite *temp2 = [CCSprite spriteWithSpriteFrameName:@"start_pressed.png"];
        
        CCMenuItemSprite *nextscene_button = [CCMenuItemSprite itemWithNormalSprite:temp1 selectedSprite:temp2 target:self selector:@selector(go_nextscene)];
        nextscene_button.position = ccp(winsize.width/2, winsize.height*0.61);
        
        CCMenu *mainmenu = [CCMenu menuWithItems:nextscene_button, nil];
        mainmenu.position = CGPointZero;
        [self addChild:mainmenu z:301];
        
        [[CCTextureCache sharedTextureCache] removeUnusedTextures];
        [[CCSpriteFrameCache sharedSpriteFrameCache] removeUnusedSpriteFrames];
    }
    
	return self;
}

-(void) go_nextscene {
    [[SimpleAudioEngine sharedEngine] playEffect:@"button4-1.caf"];
    [[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:1 scene:[ChoiceModel scene]]];
}

-(void) onEnter
{
	[super onEnter];    
}

- (void)dealloc {
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    [[CCSpriteFrameCache sharedSpriteFrameCache] removeUnusedSpriteFrames];
    
    [[CCTextureCache sharedTextureCache] removeAllTextures];
    [[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFrames];
    
    [self removeAllChildrenWithCleanup:YES];
    
    [super dealloc];
}

@end
