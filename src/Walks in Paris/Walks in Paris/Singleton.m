//
//  Singleton.m
//  Walks in Paris
//
//  Created by Andrey Zavaliy on 14.01.13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "Singleton.h"

@implementation Singleton
@synthesize Trial_Mode = _Trial_Mode;
@synthesize number_model = _number_model;
@synthesize Show_Reklama = _Show_Reklama;
@synthesize rerun_choise = _rerun_choise;

static Singleton* _sharedSingleton = nil;

+(Singleton *) sharedSingleron{
    @synchronized([Singleton class]) {
        if (!_sharedSingleton)
            _sharedSingleton = [[self alloc] init];
        
        return _sharedSingleton;
    }
    return nil;
}

+(id)alloc
{
    @synchronized([Singleton class])
    {
        NSAssert(_sharedSingleton == nil, @"Attempted to allocate a second instance of a singleton.");
        _sharedSingleton = [super alloc];
        return _sharedSingleton;
    }
    return nil;
}

-(id)init {
    self = [super init];
    if (self != nil) {
        
    }
    return self;
}

@end